//
//  OnePDFProcessController.h
//  One PDF
//
//  Created by Simon on 24/12/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <PDFKit/PDFKit.h>
#import "NSImage+Compression.h"
#include "constants.h"

@class OnePDFOutlineNodeData;

@interface OnePDFProcessController : NSObject

#pragma mark - Properties

#pragma mark State
/** A NSProgressIndicator to be updated with conversion progress */
@property (strong)            NSProgressIndicator *progress;
@property                     BOOL isRunning;

#pragma mark Node Details
/** The root tree node, filled with PDFOutlineNodeData objects wrapped in NSTreeNodes. */
@property (strong)            NSTreeNode *rootNode;
/** The containing folder path of the output file. */
@property (strong)            NSString *parentFolderPath;
/** The output file's name. */
@property (nonatomic, strong) NSString *fileName;

#pragma mark Processing Options
/** Create a single PDF (YES) or a directory with a PDF per child of the root tree node (NO). */
@property                     BOOL singlePDF;
/** Whether chapter markers are to be included in the final PDF. */
@property                     BOOL chapterMarkers;
/** Dictionary of PDF metadata (such as PDFDocumentTitleAttribute). See -[PDFDocument documentAttributes] in PDFDocument.h for more info*/
@property (strong)            NSDictionary *metaData;
/** Image quality, from 0 to 1: 0 = maximum compression, 1 = no compression */
@property                     float compression;
/** An array of UTIs that are to be excluded from processing. */
@property (strong)            NSArray *UTIsToExclude;

/** Returns whether to overwrite file at path given by NSString. If not provided, any files will be overwritten. Will be run on the main thread. */
@property (strong)            BOOL (^shouldOverwrite)(NSString*);
/** Called on error or warning. NSError may not be given, in which case the provided NSStrings (title and description) should be used. Default block uses NSLog. Will be run on the main application thread. */
@property (strong)            void (^errorBlock)(NSError*,NSString*,NSString*);

#pragma mark - Methods

#pragma mark Designated Initialiser
/** The designated initialiser. You may use init, but
    you will have to set these properties manually.
 @param rootTreeNode    *REQUIRED* The root tree node, filled with PDFOutlineNodeData objects wrapped in NSTreeNodes.
 @param fileName        *REQUIRED* The output file's name.
 @param filePath        *REQUIRED* The containing folder path of the output file.
 @param singlePDF       *REQUIRED* Create a single PDF (YES) or a directory with a PDF per child of the root tree node (NO).
 @param chapterMarkers  *REQUIRED* Whether chapter markers are to be included in the final PDF.
 @param compression     Image quality, from 0 to 1: 0 = maximum compression, 1 = no compression
 @param metaDataDict    Dictionary of PDF metadata (such as PDFDocumentTitleAttribute). See -[PDFDocument documentAttributes] in PDFDocument.h for more info
 @param progress        A NSProgressIndicator to be updated with processing progress
 @param overwriteBlock  Returns whether to overwrite file at path given by NSString. If not provided, any files will be overwritten. Will be run on the main thread.
 @param errorBlock      Called on error or warning. NSError may not be given, in which case the provided NSStrings (title and description) should be used. Default block uses NSLog. Will be run on the main application thread.
 @param UTIs            An array of UTIs that are to be excluded from processing.
 */
- (id)initWithRootTreeNode:(NSTreeNode*)rootTreeNode
                  fileName:(NSString*)fileName
          parentFolderPath:(NSString*)filePath
                 singlePDF:(BOOL)singlePDF
            chapterMarkers:(BOOL)chapterMarkers
               compression:(float)compression
        metaDataDictionary:(NSDictionary*)metaDataDict
         progressIndicator:(NSProgressIndicator*)progress
            overwriteBlock:(BOOL (^)(NSString*))overwriteBlock
                errorBlock:(void (^)(NSError*,NSString*,NSString*))errorBlock
             UTIsToExclude:(NSArray*)UTIs;

#pragma mark Class Initialisers
/** Shortcut initialiser. See initWithRootTreeNode... for more information.  */
+ (id)processControllerWithRootTreeNode:(NSTreeNode*)rootTreeNode
                               fileName:(NSString*)fileName
                       parentFolderPath:(NSString*)filePath
                              singlePDF:(BOOL)singlePDF
                         chapterMarkers:(BOOL)chapterMarkers
                            compression:(float)compression
                     metaDataDictionary:(NSDictionary*)metaDataDict
                      progressIndicator:(NSProgressIndicator*)progress
                         overwriteBlock:(BOOL (^)(NSString*))overwriteBlock
                             errorBlock:(void (^)(NSError*,NSString*,NSString*))errorBlock
                          UTIsToExclude:(NSArray*)UTIs;

/** Shortcut initialiser. See initWithRootTreeNode... for more information.  */
+ (BOOL)beginProcessWithRootTreeNode:(NSTreeNode*)rootTreeNode
                            fileName:(NSString*)fileName
                    parentFolderPath:(NSString*)filePath
                           singlePDF:(BOOL)singlePDF
                      chapterMarkers:(BOOL)chapterMarkers
                         compression:(float)compression
                  metaDataDictionary:(NSDictionary*)metaDataDict
                   progressIndicator:(NSProgressIndicator*)progress
                      overwriteBlock:(BOOL (^)(NSString*))overwriteBlock
                          errorBlock:(void (^)(NSError*,NSString*,NSString*))errorBlock
                       UTIsToExclude:(NSArray*)UTIs;
#pragma mark Processing
/** Begins processing if all required properties are set.
    Uses a GCD async thread. */
- (BOOL)beginProcess;

@end