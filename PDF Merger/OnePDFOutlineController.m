/*
 File: AppController.m
 Abstract: Application Controller object implementation.
 Version: 1.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
 */

#import "OnePDFOutlineController.h"

#import "OnePDFAppDelegate.h"
#import "OnePDFMainWindowController.h"

#import "OnePDFOutlineNodeData.h"
#import "ImageAndTextCell.h"

#define OnePDFOutlineUnselectedCellColor [NSColor colorWithDeviceWhite:153.f/255.f alpha:1.000]

@interface OnePDFOutlineController()

- (void)addNewDataToSelection:(OnePDFOutlineNodeData *)newChildData;
- (BOOL)addDirectoryChildrenForTreeNode:(NSTreeNode *)treeNode;

- (BOOL)_dragIsLocalReorder:(id <NSDraggingInfo>)info;
- (BOOL)areParentsSelectedForTreeNode:(NSTreeNode*)item;
- (BOOL)areParentsChapterMarkersForTreeNode:(NSTreeNode*)item;
- (void)outlineView:(NSOutlineView *)ov reloadChildrenForItem:(NSTreeNode*)item columnIndexes:(NSIndexSet*)col;

@property (readwrite) NSInteger pageCount;
- (NSInteger)treeNodeChildrenPageCount:(NSTreeNode*)node;
@property BOOL statusStringUpdating;

@property BOOL cancelPageCountForDeletion;


@property NSImage *dirIcon;
@property NSImage *docIcon;

- (void)restoreExpansionOfTreeNodeAndKids:(NSTreeNode*)node;

@end



@implementation OnePDFOutlineController

@synthesize outlineView = _outlineView;
@synthesize rootTreeNode = _rootTreeNode;

//Menu
@synthesize outlineViewContextMenu = _outlineViewContextMenu;

@synthesize createBookmarkMenuItem = _createBookmarkMenuItem;
@synthesize addFilesMenuItem = _addFilesMenuItem;

@synthesize menuSeperator = _menuSeperator;

@synthesize renameMenuItem = _renameMenuItem;
@synthesize expandMenuItem = _expandMenuItem;
@synthesize QLMenuItem = _QLMenuItem;
@synthesize revealInFinderMenuItem = _revealInFinderMenuItem;
@synthesize deleteMenuItem = _deleteMenuItem;

// Misc
@synthesize draggedNodes = _draggedNodes;
@synthesize iconImages = _iconImages;

@synthesize statusStringUpdating = _statusStringUpdating;

- (id)init {
    if (self = [super init]) {
        OnePDFOutlineNodeData *nodeData = [OnePDFOutlineNodeData nodeDataWithName:@"OVRoot"];
        nodeData.directory = YES;
        
        _rootTreeNode = [NSTreeNode treeNodeWithRepresentedObject:nodeData];
        
        self.dirIcon = [NSImage imageNamed:@"dirIcon"];
        self.docIcon = [NSImage imageNamed:@"docIcon"];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)coder { // We aren't actually using a coder yet, might be useful for restore...
    if (self = [super init]) {
        OnePDFOutlineNodeData *nodeData = [OnePDFOutlineNodeData nodeDataWithName:@"OVRoot"];
        nodeData.directory = YES;
        
        _rootTreeNode = [NSTreeNode treeNodeWithRepresentedObject:nodeData];
    }
    return self;
}

- (void)encodeWithCoder: (NSCoder *)coder {} // Nothing to see here...


- (void)awakeFromNib {
    // Register to get our custom type, strings, and filenames.
    [self.outlineView registerForDraggedTypes:[NSArray arrayWithObjects:LOCAL_REORDER_PASTEBOARD_TYPE, NSFilenamesPboardType, nil]];
    [self.outlineView setDraggingSourceOperationMask:NSDragOperationEvery forLocal:YES];
    [self.outlineView setDraggingSourceOperationMask:NSDragOperationEvery forLocal:NO];
    [self.outlineView setAutoresizesOutlineColumn:NO];
    [self.outlineView setVerticalMotionCanBeginDrag:YES];
    
    [self.outlineView expandItem:nil expandChildren:YES];
}

- (BOOL)allowDropOnDirectory {
    return TRUE;
}

- (BOOL)allowOnDropOnLeaf {
    return FALSE;
}

- (BOOL)allowBetweenDrop {
    return TRUE;
}

- (BOOL)onlyAcceptDropOnRoot {
    return FALSE;
}

- (BOOL)useGroupRowLook {
    return FALSE;
}

- (BOOL)allowButtonCellsToChangeSelection {
    return FALSE;
}

- (void)prepareUndoManagerWithCurrentSettings {
    [[[(OnePDFAppDelegate*)(OnePDFAppDelegate*)[NSApp delegate] undoManager] prepareWithInvocationTarget:self]
     restoreRootTreeNode:[self.rootTreeNode copy]
     selection:[self.outlineView selectedRowIndexes]
     verticalScrollDistance:[[self.outlineView.enclosingScrollView verticalScroller] doubleValue]
     prepUndoManager:YES];
}

- (void)restoreRootTreeNode:(NSTreeNode*)rootTreeNode selection:(NSIndexSet*)selection verticalScrollDistance:(double)vScroll prepUndoManager:(BOOL)prepUndoMan {
    if (self.outlineView.isARowBeingEdited) [[[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController] mainWindow] makeFirstResponder:nil];
    if (prepUndoMan) [self prepareUndoManagerWithCurrentSettings];
    [self replaceRootNodeWithNode:rootTreeNode];
    [self.outlineView selectRowIndexes:selection byExtendingSelection:NO];
    [[self.outlineView.enclosingScrollView verticalScroller] setDoubleValue:vScroll];
    [[[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController] mainWindow] makeFirstResponder:self.outlineView];
}
                                                            
- (void)replaceRootNodeWithNode:(NSTreeNode*)newNode {
    _rootTreeNode = newNode;
    [self updateStatusStringPages];
    [self.outlineView reloadData];
    [self restoreExpansionOfTreeNodeAndKids:self.rootTreeNode];
    
    //if ([[self.rootTreeNode childNodes] count]<1)
    //    [[NSNotificationCenter defaultCenter] postNotificationName:ONEPDF_OUTLINE_EMPTY_NOTIFICATION object:self];
}

- (void)restoreExpansionOfTreeNodeAndKids:(NSTreeNode*)node {
    if ([node isEqualTo:self.rootTreeNode])
        [self.outlineView expandItem:nil expandChildren:NO];
    else if ([[node representedObject] isExpanded])
        [self.outlineView expandItem:node expandChildren:NO];
    
    for (NSTreeNode *childNode in [node childNodes])
        [self restoreExpansionOfTreeNodeAndKids:childNode];
}

- (void)addBookmark:(id)sender {
    [self prepareUndoManagerWithCurrentSettings];
    [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Create Bookmark", nil)];
    // Create a new bookmark, and insert it into our tree structure
    OnePDFOutlineNodeData *childNodeData = [[OnePDFOutlineNodeData alloc] initWithName:NSLocalizedString(@"New Bookmark", nil)];
    [childNodeData setBookmark:YES];
    [self addNewDataToSelection:childNodeData];
    [childNodeData setImage:nil];
}

- (void)outlineViewSelectionDidChange:(NSNotification *)notification {
    // We'll call a refresh of the QuickLook panel, so it changes when you change the selection...
    [self refreshPreviewPanel:[QLPreviewPanel sharedPreviewPanel]];
}

- (void)deleteSelections:(id)sender {
    if (self.outlineView.isARowBeingEdited || [(OnePDFAppDelegate*)[NSApp delegate] processing]) return;
    if (self.statusStringUpdating) {
        self.cancelPageCountForDeletion = YES;
        return;
    }
    [self.outlineView beginUpdates];
    if([self isASingleRowClicked]) {
        NSTreeNode *node = [self.outlineView itemAtRow:[self.outlineView clickedRow]];
        [self prepareUndoManagerWithCurrentSettings];
        [[(OnePDFAppDelegate*)(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:[NSString stringWithFormat:
                                                       NSLocalizedString(@"Remove \"%@\"", nil),
                                                       [(OnePDFOutlineNodeData*)[node representedObject] name]]];
        NSTreeNode *parent = [node parentNode];
        NSMutableArray *childNodes = [parent mutableChildNodes];
        NSInteger index = [childNodes indexOfObject:node];
        [childNodes removeObjectAtIndex:index];
        if (parent == self.rootTreeNode) {
            parent = nil; // NSOutlineView doesn't know about our root node, so we use nil
        }
        [self.outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:index] inParent:parent withAnimation:NSTableViewAnimationEffectFade | NSTableViewAnimationSlideLeft];
    } else {
        [[self.outlineView selectedRowIndexes] enumerateIndexesWithOptions:NSEnumerationReverse usingBlock:^(NSUInteger row, BOOL *stop) {
            NSTreeNode *node = [self.outlineView itemAtRow:row];
            [self prepareUndoManagerWithCurrentSettings];
            [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:[NSString stringWithFormat:
                                                           NSLocalizedString(@"Remove \"%@\"", nil),
                                                           [(OnePDFOutlineNodeData*)[node representedObject] name]]];
            NSTreeNode *parent = [node parentNode];
            NSMutableArray *childNodes = [parent mutableChildNodes];
            NSInteger index = [childNodes indexOfObject:node];
            [childNodes removeObjectAtIndex:index];
            if (parent == self.rootTreeNode) {
                parent = nil; // NSOutlineView doesn't know about our root node, so we use nil
            }
            [self.outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:index] inParent:parent withAnimation:NSTableViewAnimationEffectFade | NSTableViewAnimationSlideLeft];
        }];
    }
    [self.outlineView endUpdates];
    if ([[self.rootTreeNode childNodes] count] < 1 && [[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController] respondsToSelector:@selector(allElementsRemovedFromTree)])
        [[NSNotificationCenter defaultCenter] postNotificationName:ONEPDF_OUTLINE_EMPTY_NOTIFICATION object:self];
    [self updateStatusStringPages];
}

- (IBAction)sortData:(id)sender {
    // Save and restore the selection
    NSMutableArray *items = [NSMutableArray array];
    [[self.outlineView selectedRowIndexes] enumerateIndexesUsingBlock:^(NSUInteger row, BOOL *stop) {
        [items addObject:[self.outlineView itemAtRow:row]];
    }];
    
    // Create a sort descriptor to do the sorting. Use a 'nil' key to sort on the objects themselves. This will by default use the method "compare:" on the representedObjects in the NSTreeNode.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
    [self.rootTreeNode sortWithSortDescriptors:[NSArray arrayWithObject:sortDescriptor] recursively:YES];
    
    [self.outlineView reloadData];
    
    // Reselect the original selected items
    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
    for (NSTreeNode *node in items) {
        NSInteger row = [self.outlineView rowForItem:node];
        if (row != -1) {
            [indexes addIndex:row];
        }
    }
    [self.outlineView selectRowIndexes:indexes byExtendingSelection:NO];
}

- (BOOL)validateMenuItem:(NSMenuItem *)menuItem {
    if ([menuItem action] == @selector(deleteSelections:)) {
        // The delete selection item should be disabled if nothing is selected.
        if ([[self.outlineView selectedRowIndexes] count] > 0 && [self.outlineView clickedRow] > 0) {
            return YES;
        } else {
            return NO;
        }
    }
    return YES;
}

#pragma mark - NSOutlineView data source methods. (The required ones)

// The NSOutlineView uses 'nil' to indicate the root item. We return our root tree node for that case.
- (NSArray *)childrenForItem:(id)item {
    if (item == nil)
        item = self.rootTreeNode;
    return [item childNodes];
}

- (id)outlineView:(NSOutlineView *)outlineView child:(NSInteger)index ofItem:(id)item {
    // 'item' may potentially be nil for the root item.
    NSArray *children = [self childrenForItem:item];
    // This will return an NSTreeNode with our model object as the representedObject
    return [children objectAtIndex:index];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView isItemExpandable:(id)item {
    // 'item' will always be non-nil. It is an NSTreeNode, since those are always the objects we give NSOutlineView. We access our model object from it.
    OnePDFOutlineNodeData *nodeData = [item representedObject];
    // We can expand items if the model tells us it is a directory AND it's selected.
    return nodeData.directory && nodeData.selected;
}

- (NSInteger)outlineView:(NSOutlineView *)outlineView numberOfChildrenOfItem:(id)item {
    // 'item' may potentially be nil for the root item.
    NSArray *children = [self childrenForItem:item];
    return [children count];
}

#pragma mark Cell Editing

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldEditTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    NSEvent *currEvent = [NSApp currentEvent];
    NSPoint location = [NSEvent mouseLocation];
    
    if ([[tableColumn identifier] isEqualToString:COLUMNID_NAME]) {
        //NSCell *cell = [self.outlineView preparedCellAtColumn:[[self.outlineView tableColumns] indexOfObject:tableColumn] row:[self.outlineView rowForItem:item]];
        //NSLog(@"%@",cell.frame);
        
        NSRect cellFrame = [self.outlineView frameOfCellAtColumn:[[self.outlineView tableColumns] indexOfObject:tableColumn] row:[self.outlineView rowForItem:item]];
        NSRect cellFrameInWindow = [self.outlineView convertRect:cellFrame toView:nil];
        NSRect cellFrameOnScreen = [[self.outlineView window] convertRectToScreen:cellFrameInWindow];
        if (NSPointInRect(location, cellFrameOnScreen)) {
            self.outlineView.isARowBeingEdited = YES;
            return YES;
        } else if ([currEvent type] == NSKeyDown) {
            NSString* keys = [currEvent charactersIgnoringModifiers];
            unichar firstCharOfKeys = [keys characterAtIndex:0];
            if (firstCharOfKeys==NSEnterCharacter ||
                firstCharOfKeys==NSCarriageReturnCharacter ||
                firstCharOfKeys==NSNewlineCharacter) {
                self.outlineView.isARowBeingEdited = YES;
                return YES;
            }
        }
            
    }
    return NO;
}

- (void)controlTextDidBeginEditing:(NSNotification *)obj {
    self.outlineView.isARowBeingEdited = YES;
}

- (void)controlTextDidEndEditing:(NSNotification *)obj {
    self.outlineView.isARowBeingEdited = NO;
}

- (id)outlineView:(NSOutlineView *)outlineView objectValueForTableColumn:(NSTableColumn *)tableColumn byItem:(id)item {
    id objectValue = nil;
    OnePDFOutlineNodeData *nodeData = [item representedObject];
    
    // The return value from this method is used to configure the state of the items cell via setObjectValue:
    if ([[tableColumn identifier] isEqualToString:COLUMNID_IS_CHAPTER_MARKER]) {
        // If it's a chapter marker and its parent is a chapter marker
        /*if (nodeData.chapterMarker&&[self areParentsChapterMarkersForTreeNode:item]) {
            objectValue = [NSNumber numberWithBool:YES];
            nodeData.chapterMarker = YES;
        } else {
            objectValue = [NSNumber numberWithBool:NO];
            nodeData.chapterMarker = NO;
        }*/
        objectValue = [NSNumber numberWithBool:nodeData.chapterMarker];
    } else if ((tableColumn == nil) || [[tableColumn identifier] isEqualToString:COLUMNID_NAME]) {
        objectValue = nodeData.name;
    } else if ([[tableColumn identifier] isEqualToString:COLUMNID_SELECTED]) {
        if (nodeData.selected && [self areParentsSelectedForTreeNode:item]) {
            objectValue = [NSNumber numberWithBool:YES];
        } else {
            objectValue = [NSNumber numberWithBool:NO];
        }
    }
    
    return objectValue;
}

// Optional method: needed to allow editing.
- (void)outlineView:(NSOutlineView *)ov setObjectValue:(id)object forTableColumn:(NSTableColumn *)tableColumn byItem:(id)item  {
    OnePDFOutlineNodeData *nodeData = [item representedObject];
    
    if (![object isKindOfClass:[NSAttributedString class]])
        [self prepareUndoManagerWithCurrentSettings];
    NSString *actionName = nil;
    
    if ([[tableColumn identifier] isEqualToString:COLUMNID_IS_CHAPTER_MARKER]) {
        NSUInteger items = 0;
        if ([[ov selectedRowIndexes] count]>1) {
            NSIndexSet *sel = [ov selectedRowIndexes];
            NSUInteger currentIndex = [sel firstIndex];
            while (currentIndex != NSNotFound) {
                OnePDFOutlineNodeData *obj = [[ov itemAtRow:currentIndex] representedObject];
                if (!obj.bookmark)
                    obj.chapterMarker = [object boolValue];
                [ov reloadItem:[ov itemAtRow:currentIndex] reloadChildren:[ov isItemExpanded:item]];
                
                currentIndex = [sel indexGreaterThanIndex:currentIndex];
                items++;
            }
        } else {
            nodeData.chapterMarker = [object boolValue];
            [ov reloadItem:item reloadChildren:YES];
            items++;
        }
        
        NSString *nameOrNumberOfItems = items < 2 ?
        [NSString stringWithFormat:NSLocalizedString(@"\"%@\"", nil), [nodeData name]] :
        [NSString stringWithFormat:NSLocalizedString(@"%d Items", nil), items];
        NSString *markOrUnmark = [object boolValue] ? NSLocalizedString(@"Mark %@ as Chapter Marker", nil) : NSLocalizedString(@"Unmark %@ as Chapter Marker", nil);
        actionName = [NSString stringWithFormat:markOrUnmark, nameOrNumberOfItems];
    } else if ([[tableColumn identifier] isEqualToString:COLUMNID_NAME] || tableColumn == nil) {
        if ([object isKindOfClass:[NSAttributedString class]]) {
            nodeData.name = [object string];
        }
        else if ([object isKindOfClass:[NSString class]]) {
            nodeData.name = object;
            actionName = [NSString stringWithFormat:NSLocalizedString(@"Rename \"%@\"", nil), [nodeData name]];
        }
    } else if ([[tableColumn identifier] isEqualToString:COLUMNID_SELECTED]) {
        NSUInteger items = 0;
        if ([[ov selectedRowIndexes] count]>1) {
            NSIndexSet *sel = [ov selectedRowIndexes];
            NSUInteger currentIndex = [sel lastIndex];
            while (currentIndex != NSNotFound) {
                OnePDFOutlineNodeData *obj = [[ov itemAtRow:currentIndex] representedObject];
                
                obj.selected = [object boolValue];
                [ov collapseItem:[ov itemAtRow:currentIndex]];
                [ov reloadItem:[ov itemAtRow:currentIndex] reloadChildren:[ov isItemExpanded:item]];
                
                // We are parsing in reverse so that if a directory's child is
                // deselected along with its parent directory, it doesn't collapse
                // the parent and thereby cause the directory's child to be hidden,
                // which stops it from being deselected...
                currentIndex = [sel indexLessThanIndex:currentIndex];
                items++;
            }
        } else {
            nodeData.selected = [object boolValue];
            [ov collapseItem:item];
            [ov reloadItem:item reloadChildren:YES];
            items++;
        }
        [self updateStatusStringPages];
        NSString *selectOrDeselect = [object boolValue] ? NSLocalizedString(@"Include", nil) : NSLocalizedString(@"Exclude", nil);
        NSString *nameOrNumberOfItems = items < 2 ?
        [NSString stringWithFormat:NSLocalizedString(@"\"%@\"", nil), [nodeData name]] :
        [NSString stringWithFormat:NSLocalizedString(@"%d Items", nil), items];
        actionName = [NSString stringWithFormat:@"%@ %@", selectOrDeselect, nameOrNumberOfItems];
    }
    if (![object isKindOfClass:[NSAttributedString class]])
        [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:actionName];
}

- (BOOL)areParentsSelectedForTreeNode:(NSTreeNode*)item {
    if([item parentNode]) {
        if (![[[item parentNode] representedObject] isSelected])
            return false;
        return [self areParentsSelectedForTreeNode:[item parentNode]];
    } else
        return true;
}

- (BOOL)areParentsChapterMarkersForTreeNode:(NSTreeNode*)item {
    if([item parentNode]) {
        if (![[[item parentNode] representedObject] isChapterMarker])
            return false;
        return [self areParentsSelectedForTreeNode:[item parentNode]];
    } else
        return true;
}

- (void)outlineView:(NSOutlineView *)ov reloadChildrenForItem:(NSTreeNode*)item columnIndexes:(NSIndexSet*)col {
    NSMutableIndexSet *indexes = [[NSMutableIndexSet alloc] init];
    for (NSUInteger i=0; i!=[[item childNodes] count]; i++) {
        if([[[[(NSTreeNode*)item childNodes] objectAtIndex:i] representedObject] isDirectory])
            [self outlineView:ov reloadChildrenForItem:[[(NSTreeNode*)item childNodes] objectAtIndex:i] columnIndexes:col];
        [indexes addIndex:[ov rowForItem:[[(NSTreeNode*)item childNodes] objectAtIndex:i]]];
    }
    [ov reloadDataForRowIndexes:indexes columnIndexes:col];
}


- (IBAction)editClickedRow:(id)sender {
    self.outlineView.isARowBeingEdited = YES;
    [self.outlineView editColumn:1 row:[self.outlineView clickedRow] withEvent:NULL select:YES];
}


- (IBAction)expandClickedRow:(id)sender {
    if ([[self.outlineView selectedRowIndexes] count]>1) {
        NSIndexSet *sel = [self.outlineView selectedRowIndexes];
        NSUInteger currentIndex = [sel firstIndex];
        while (currentIndex != NSNotFound) {
            [self.outlineView expandItem:[self.outlineView itemAtRow:currentIndex]];
            currentIndex = [sel indexGreaterThanIndex:currentIndex];
        }
    } else {
        [self.outlineView expandItem:[self.outlineView itemAtRow:[self.outlineView clickedRow]]];
    }
}


// We can return a different cell for each row, if we want
- (NSCell *)outlineView:(NSOutlineView *)ov dataCellForTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    // If we return a cell for the 'nil' tableColumn, it will be used as a "full width" cell and span all the columns
    /*if ([self useGroupRowLook] && tableColumn == nil) {
        SimpleNodeData *nodeData = [item representedObject];
        if (nodeData.container) {
            // We want to use the cell for the name column, but we could construct a new cell if we wanted to, or return a different cell for each row.
            return [[self.outlineView tableColumnWithIdentifier:COLUMNID_NAME] dataCell];
        }
    }*/
    if ([[item representedObject] isBookmark] && [[tableColumn identifier] isEqualToString:COLUMNID_IS_CHAPTER_MARKER]) {
        NSButtonCell *cell = [NSButtonCell new];
        [cell setImagePosition:NSNoImage];
        [cell setEnabled:NO];
        [cell setBordered:NO];
        [cell setTitle:@""];
        return cell;
    }
    else
        return [tableColumn dataCell];
}

// To get the "group row" look, we implement this method.
- (BOOL)outlineView:(NSOutlineView *)outlineView isGroupItem:(id)item {
    OnePDFOutlineNodeData *nodeData = [item representedObject];
    return nodeData.chapterMarker && [self useGroupRowLook];
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldExpandItem:(id)item {
    // Query our model for the answer to this question
    OnePDFOutlineNodeData *nodeData = [item representedObject];
    // We can expand items if the model tells us it is a directory.
    if (nodeData.directory && nodeData.selected)
        return nodeData.expanded = YES;
    return NO;
}

- (BOOL)outlineView:(NSOutlineView *)outlineView shouldCollapseItem:(id)item {
    OnePDFOutlineNodeData *nodeData = [item representedObject];
    nodeData.expanded = NO;
    return YES;
}

- (void)outlineView:(NSOutlineView *)outlineView willDisplayOutlineCell:(NSCell *)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    // White and embossed arrows
    [cell setBackgroundStyle:NSBackgroundStyleLowered];
    // Or, to use an image...
    //[cell setImage:closedImage];
    //[cell setAlternateImage:openImage];
}

- (void)outlineView:(NSOutlineView *)outlineView willDisplayCell:(NSCell *)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    OnePDFOutlineNodeData *nodeData = [item representedObject];
    if ((tableColumn == nil) || [[tableColumn identifier] isEqualToString:COLUMNID_NAME]) {
        // Get the image for each item using iconForFileType
        if (nodeData.image == nil) {
            if (nodeData.directory) {
                //nodeData.image = [[NSWorkspace sharedWorkspace] iconForFileType:NSFileTypeForHFSTypeCode(kGenericFolderIcon)];
                nodeData.image = self.dirIcon;
            } else if (nodeData.bookmark) {
                nodeData.image = nil;
            } else {
                //nodeData.image = [[NSWorkspace sharedWorkspace] iconForFileType:([nodeData kind]?[nodeData kind]:@"")];                
                NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:nodeData.path error:nil];
                if (/*nodeData.numberOfPages>ONEPDF_LARGE_PAGE_AMOUNT ||*/ [attrs fileSize] > ONEPDF_LARGE_FILE_SIZE)
                    nodeData.image = [NSImage imageNamed:@"docIcon-warning"];
                else
                    nodeData.image = self.docIcon;
            }
        }
        // We know that the cell at this column is our image and text cell, so grab it
        ImageAndTextCell *imageAndTextCell = (ImageAndTextCell *)cell;
        // Set the image here since the value returned from outlineView:objectValueForTableColumn:... didn't specify the image part...
        [imageAndTextCell setImage:nodeData.image];
        
        // Handles what cell text looks like when cells are not selected...
        NSMutableAttributedString *attrString = [[NSMutableAttributedString alloc] initWithString:[cell stringValue]];
        if (!nodeData.selected || [[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController] isUTIFilteredOut:nodeData.kind]) {
            //[attrString addAttribute:NSStrikethroughStyleAttributeName value:(NSNumber *)kCFBooleanTrue range:NSMakeRange(0, [attrString length])];
            [attrString addAttribute:NSObliquenessAttributeName value:[NSNumber numberWithFloat:0.2] range:NSMakeRange(0, [attrString length])];
            [attrString addAttribute:NSForegroundColorAttributeName value:OnePDFOutlineUnselectedCellColor range:NSMakeRange(0, [attrString length])];
        } else if (nodeData.chapterMarker || nodeData.bookmark) {
            [attrString addAttribute:NSFontAttributeName value:[NSFont fontWithName:@"Helvetica-Bold" size:12] range:NSMakeRange(0, [attrString length])];
        }
        [cell setAttributedStringValue:attrString];
        [outlineView setNeedsDisplayInRect:[outlineView frameOfOutlineCellAtRow:[outlineView rowForItem:item]]];
         
    } else if ([[tableColumn identifier] isEqualToString:COLUMNID_IS_CHAPTER_MARKER]||[[tableColumn identifier] isEqualToString:COLUMNID_SELECTED]) {
        if ([cell isKindOfClass:[NSButtonCell class]]) {
            NSInteger row = [self.outlineView rowForItem:item];
            if([self.outlineView isRowBeingHovered:row]||nodeData.chapterMarker||[self outlineView:outlineView isRowSelected:row]) {
                [(NSButtonCell*)cell setImagePosition: NSImageOnly];
                [(NSButtonCell*)cell setEnabled:YES];
            } else {
                [(NSButtonCell*)cell setImagePosition: NSNoImage];
                [(NSButtonCell*)cell setEnabled:NO];
            }
        }
    }
}

- (BOOL)outlineView:(NSOutlineView *)ov isRowSelected:(NSInteger)row {
    if ([ov clickedRow]==row || [ov selectedRow]==row) return YES;
    NSUInteger currentIndex = [[ov selectedRowIndexes] firstIndex];
    while (currentIndex != NSNotFound) {
        if (currentIndex==row) return YES;
        currentIndex = [[ov selectedRowIndexes] indexGreaterThanIndex:currentIndex];
    }
    return NO;
}

// If there is only one row in the proposed selection, see if it's already selected and, if so, discard the new selection.
// This was written to stop the selection of a button cell from changing the selection...
- (NSIndexSet *)outlineView:(NSOutlineView *)ov selectionIndexesForProposedSelection:(NSIndexSet *)proposedSelectionIndexes {
    if ([proposedSelectionIndexes count]==1) {
        NSUInteger currentIndex = [[ov selectedRowIndexes] firstIndex];
        while (currentIndex != NSNotFound) {
            if (currentIndex==[proposedSelectionIndexes firstIndex])
                return [ov selectedRowIndexes];
            currentIndex = [[ov selectedRowIndexes] indexGreaterThanIndex:currentIndex];
        }
    }
    return proposedSelectionIndexes;
}

- (BOOL)outlineView:(NSOutlineView *)ov shouldTrackCell:(NSCell *)cell forTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    // return YES;
    // We want to allow tracking for all the button cells, even if we don't allow selecting that particular row.
    if ([cell isKindOfClass:[NSButtonCell class]]) {
        // We can also take a peek and make sure that the part of the cell clicked is an area that is normally tracked. Otherwise, clicking outside of the checkbox may make it check the checkbox
        NSRect cellFrame = [self.outlineView frameOfCellAtColumn:[[self.outlineView tableColumns] indexOfObject:tableColumn] row:[self.outlineView rowForItem:item]];
        NSUInteger hitTestResult = [cell hitTestForEvent:[NSApp currentEvent] inRect:cellFrame ofView:self.outlineView];
        if ((hitTestResult & NSCellHitTrackableArea) != 0) {
            return YES;
        } else {
            return NO;
        }
    } else {
        // Only allow tracking on selected rows. This is what NSTableView does by default.
        return [self.outlineView isRowSelected:[self.outlineView rowForItem:item]];
    }
}

/* In 10.7 multiple drag images are supported by using this delegate method. */
- (id <NSPasteboardWriting>)outlineView:(NSOutlineView *)outlineView pasteboardWriterForItem:(id)item {
    return (id <NSPasteboardWriting>)[item representedObject];
}

/* Setup a local reorder. */
- (void)outlineView:(NSOutlineView *)outlineView draggingSession:(NSDraggingSession *)session willBeginAtPoint:(NSPoint)screenPoint forItems:(NSArray *)draggedItems {
    self.draggedNodes = draggedItems;
    [session.draggingPasteboard setData:[NSData data] forType:LOCAL_REORDER_PASTEBOARD_TYPE];
}

- (void)outlineView:(NSOutlineView *)outlineView draggingSession:(NSDraggingSession *)session endedAtPoint:(NSPoint)screenPoint operation:(NSDragOperation)operation {
    // If the session ended in the trash, then delete all the items
    if (operation == NSDragOperationDelete) {
        [self.outlineView beginUpdates];
        
        [self.draggedNodes enumerateObjectsWithOptions:NSEnumerationReverse usingBlock:^(id node, NSUInteger index, BOOL *stop) {
            id parent = [node parentNode];
            NSMutableArray *children = [parent mutableChildNodes];
            NSInteger childIndex = [children indexOfObject:node];
            [children removeObjectAtIndex:childIndex];
            [self.outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:childIndex] inParent:parent == self.rootTreeNode ? nil : parent withAnimation:NSTableViewAnimationEffectFade];
        }];
        
        [self.outlineView endUpdates];
    }
    
    self.draggedNodes = nil;
}

- (BOOL)treeNode:(NSTreeNode *)treeNode isDescendantOfNode:(NSTreeNode *)parentNode {
    while (treeNode != nil) {
        if (treeNode == parentNode) {
            return YES;
        }
        treeNode = [treeNode parentNode];
    }
    return NO;
}

- (BOOL)_dragIsLocalReorder:(id <NSDraggingInfo>)info {
    // It is a local drag if the following conditions are met:
    if ([info draggingSource] == self.outlineView) {
        // We were the source
        if (self.draggedNodes != nil) {
            // Our nodes were saved off
            if ([[info draggingPasteboard] availableTypeFromArray:[NSArray arrayWithObject:LOCAL_REORDER_PASTEBOARD_TYPE]] != nil) {
                // Our pasteboard marker is on the pasteboard
                return YES;
            }
        }
    }
    return NO;
}

- (NSDragOperation)outlineView:(NSOutlineView *)ov validateDrop:(id <NSDraggingInfo>)info proposedItem:(id)item proposedChildIndex:(NSInteger)childIndex {
    // To make it easier to see exactly what is called, uncomment the following line:
    //    NSLog(@"outlineView:validateDrop:proposedItem:%@ proposedChildIndex:%ld", item, (long)childIndex);
    
    // This method validates whether or not the proposal is a valid one.
    // We start out by assuming that we will do a "generic" drag operation, which means we are accepting the drop. If we return NSDragOperationNone, then we are not accepting the drop.
    NSDragOperation result = NSDragOperationGeneric;
    
    if ([self onlyAcceptDropOnRoot]) {
        // We are going to accept the drop, but we want to retarget the drop item to be "on" the entire outlineView
        [self.outlineView setDropItem:nil dropChildIndex:NSOutlineViewDropOnItemIndex];
    } else {
        // Check to see what we are proposed to be dropping on
        NSTreeNode *targetNode = item;
        // A target of "nil" means we are on the main root tree
        if (targetNode == nil) {
            targetNode = self.rootTreeNode;
        }
        OnePDFOutlineNodeData *nodeData = [targetNode representedObject];
        if (nodeData.directory) {
            // See if we allow dropping "on" or "between"
            if (childIndex == NSOutlineViewDropOnItemIndex) {
                if (![self allowDropOnDirectory]) {
                    // Refuse to drop on a container if we are not allowing that, and refuse a drop on a bookmark.
                    result = NSDragOperationNone;
                }
            } else {
                if (![self allowBetweenDrop]) {
                    // Refuse to drop between an item if we are not allowing that
                    result = NSDragOperationNone;
                }
            }
        } else {
            // The target node is not a container, but a leaf. See if we allow dropping on a leaf. If we don't, refuse the drop (we may get called again with a between)
            if (childIndex == NSOutlineViewDropOnItemIndex && ![self allowOnDropOnLeaf]) {
                result = NSDragOperationNone;
            }
        }
        
        // If we are allowing the drop, we see if we are draggng from ourselves and dropping into a descendent, which wouldn't be allowed...
        if (result != NSDragOperationNone) {
            // Indicate that we will animate the drop items to their final location
            info.animatesToDestination = YES;
            if ([self _dragIsLocalReorder:info]) {
                if (targetNode != self.rootTreeNode) {
                    for (NSTreeNode *draggedNode in self.draggedNodes) {
                        if ([self treeNode:targetNode isDescendantOfNode:draggedNode]) {
                            // Yup, it is, refuse it.
                            result = NSDragOperationNone;
                            break;
                        }
                    }
                }
            }
        }
    }
    
    // To see what we decide to return, uncomment this line
    //    NSLog(result == NSDragOperationNone ? @" - Refusing drop" : @" + Accepting drop");
    
    return result;
}

- (void)_performInsertWithDragInfo:(id <NSDraggingInfo>)info parentNode:(NSTreeNode *)parentNode childIndex:(NSInteger)childIndex {
    // NSOutlineView's root is nil
    id outlineParentItem = parentNode == self.rootTreeNode ? nil : parentNode;
    NSMutableArray *childNodeArray = [parentNode mutableChildNodes];
    NSInteger outlineColumnIndex = [[self.outlineView tableColumns] indexOfObject:[self.outlineView outlineTableColumn]];
    
    [self.outlineView expandItem:parentNode];
    [self prepareUndoManagerWithCurrentSettings];
    [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Add Items", nil)];
    
    // Enumerate all items dropped on us and create new model objects for them
    NSArray *classes = [NSArray arrayWithObject:[OnePDFOutlineNodeData class]];
    __block NSInteger insertionIndex = childIndex;
    [info enumerateDraggingItemsWithOptions:0 forView:self.outlineView classes:classes searchOptions:nil usingBlock:^(NSDraggingItem *draggingItem, NSInteger index, BOOL *stop) {
        OnePDFOutlineNodeData *newNodeData = (OnePDFOutlineNodeData *)draggingItem.item;
        // Wrap the model object in a tree node
        NSTreeNode *treeNode = [NSTreeNode treeNodeWithRepresentedObject:newNodeData];
        
        BOOL shouldAddToModal = [(OnePDFAppDelegate*)[NSApp delegate] isUTISupported:(__bridge CFStringRef)(newNodeData.kind)];
        
        if (UTTypeConformsTo((__bridge CFStringRef)(newNodeData.kind), kUTTypePDF)) {
            PDFDocument *doc = [[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:newNodeData.path]];
            if ([doc isLocked]) {
                if([(OnePDFAppDelegate*)[NSApp delegate] handleUnlockForDocument:doc]) newNodeData.PDF = doc;
                else shouldAddToModal = NO;
            }
        }
        if ([(OnePDFAppDelegate*)[NSApp delegate] isOfficeDoc:(__bridge CFStringRef)newNodeData.kind]) {
            [(OnePDFAppDelegate*)[NSApp delegate] performSelector:@selector(officeDocumentImportAttempt) withObject:nil afterDelay:0.05];
        }
        if (!shouldAddToModal) {
            insertionIndex++;
            return;
        }
        // Check for newly dropped directory, and add directory contents if it is.
        [self addDirectoryChildrenForTreeNode:treeNode];
        
        // Add it to the model
        [childNodeArray insertObject:treeNode atIndex:insertionIndex];
        [self.outlineView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:insertionIndex] inParent:outlineParentItem withAnimation:NSTableViewAnimationEffectGap];
        // Update the final frame of the dragging item
        NSInteger row = [self.outlineView rowForItem:treeNode];
        draggingItem.draggingFrame = [self.outlineView frameOfCellAtColumn:outlineColumnIndex row:row];
        
        // Insert all children one after another
        insertionIndex++;
    }];
    [self updateStatusStringPages];
}

- (void)_performDragReorderWithDragInfo:(id <NSDraggingInfo>)info parentNode:(NSTreeNode *)newParent childIndex:(NSInteger)childIndex {
    // We will use the dragged nodes we saved off earlier for the objects we are actually moving
    NSAssert(self.draggedNodes != nil, @"self.draggedNodes should be valid");
    
    NSMutableArray *childNodeArray = [newParent mutableChildNodes];
    
    [self.outlineView expandItem:newParent];
    [self prepareUndoManagerWithCurrentSettings];
    [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Rearrange Items", nil)];
    
    // We want to enumerate all things in the pasteboard. To do that, we use a generic NSPasteboardItem class
    NSArray *classes = [NSArray arrayWithObject:[NSPasteboardItem class]];
    __block NSInteger insertionIndex = childIndex;
    [info enumerateDraggingItemsWithOptions:0 forView:self.outlineView classes:classes searchOptions:nil usingBlock:^(NSDraggingItem *draggingItem, NSInteger index, BOOL *stop) {
        // We ignore the draggingItem.item -- it is an NSPasteboardItem. We only care about the index. The index is deterministic, and can directly be used to look into the initial array of dragged items.
        NSTreeNode *draggedTreeNode = [self.draggedNodes objectAtIndex:index];
        
        // Remove this node from its old location
        NSTreeNode *oldParent = [draggedTreeNode parentNode];
        NSMutableArray *oldParentChildren = [oldParent mutableChildNodes];
        NSInteger oldIndex = [oldParentChildren indexOfObject:draggedTreeNode];
        [oldParentChildren removeObjectAtIndex:oldIndex];
        // Tell the table it is going away; make it pop out with NSTableViewAnimationEffectNone, as we will animate the draggedItem to the final target location.
        // Don't forget that NSOutlineView uses 'nil' as the root parent.
        [self.outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:oldIndex] inParent:oldParent == self.rootTreeNode ? nil : oldParent withAnimation:NSTableViewAnimationEffectNone];
        
        // Insert this node into the new location and new parent
        if (oldParent == newParent) {
            // Moving it from within the same parent! Account for the remove, if it is past the oldIndex
            if (insertionIndex > oldIndex) {
                insertionIndex--; // account for the remove
            }
        }
        [childNodeArray insertObject:draggedTreeNode atIndex:insertionIndex];
        
        // Tell NSOutlineView about the insertion; let it leave a gap for the drop animation to come into place
        [self.outlineView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:insertionIndex] inParent:newParent == self.rootTreeNode ? nil : newParent withAnimation:NSTableViewAnimationEffectGap];
        
        insertionIndex++;
    }];
    
    // Now that the move is all done (according to the table), update the draggingFrames for the all the items we dragged. -frameOfCellAtColumn:row: gives us the final frame for that cell
    NSInteger outlineColumnIndex = [[self.outlineView tableColumns] indexOfObject:[self.outlineView outlineTableColumn]];
    [info enumerateDraggingItemsWithOptions:0 forView:self.outlineView classes:classes searchOptions:nil usingBlock:^(NSDraggingItem *draggingItem, NSInteger index, BOOL *stop) {
        NSTreeNode *draggedTreeNode = [self.draggedNodes objectAtIndex:index];
        NSInteger row = [self.outlineView rowForItem:draggedTreeNode];
        draggingItem.draggingFrame = [self.outlineView frameOfCellAtColumn:outlineColumnIndex row:row];
    }];
    
}

- (BOOL)outlineView:(NSOutlineView *)ov acceptDrop:(id <NSDraggingInfo>)info item:(id)item childIndex:(NSInteger)childIndex {
    NSTreeNode *targetNode = item;
    // A target of "nil" means we are on the main root tree
    if (targetNode == nil) {
        targetNode = self.rootTreeNode;
    }
    OnePDFOutlineNodeData *nodeData = [targetNode representedObject];
    
    // Determine the parent to insert into and the child index to insert at.
    if (!nodeData.directory) {
        // If our target is a leaf, and we are dropping on it
        if (childIndex == NSOutlineViewDropOnItemIndex) {
            // If we are dropping on a leaf, we will have to turn it into a container node
            nodeData.directory = YES;
            childIndex = 0;
        } else {
            // We will be dropping on the item's parent at the target index of this child, plus one
            NSTreeNode *oldTargetNode = targetNode;
            targetNode = [targetNode parentNode];
            childIndex = [[targetNode childNodes] indexOfObject:oldTargetNode] + 1;
        }
    } else {
        if (childIndex == NSOutlineViewDropOnItemIndex) {
            // Insert it at the start, if we were dropping on it
            childIndex = [[targetNode childNodes] count];
        }
    }
    
    // Group all insert or move animations together
    [self.outlineView beginUpdates];
    // If the source was ourselves, we use our dragged nodes and do a reorder
    if ([self _dragIsLocalReorder:info]) {
        [self _performDragReorderWithDragInfo:info parentNode:targetNode childIndex:childIndex];
    } else {
        [self _performInsertWithDragInfo:info parentNode:targetNode childIndex:childIndex];
    }
    [self.outlineView endUpdates];
    
    // Return YES to indicate we were successful with the drop. Otherwise, it would slide back the drag image.
    return YES;
}

/* Multi-item dragging destiation support.
 */
- (void)outlineView:(NSOutlineView *)outlineView updateDraggingItemsForDrag:(id <NSDraggingInfo>)draggingInfo {
    // If the source is ourselves, then don't do anything. If it isn't, we update things
    if (![self _dragIsLocalReorder:draggingInfo]) {
        // We will be doing an insertion; update the dragging items to have an appropriate image
        NSArray *classes = [NSArray arrayWithObject:[OnePDFOutlineNodeData class]];
        
        // Create a copied temporary cell to draw to images
        NSTableColumn *tableColumn = [self.outlineView outlineTableColumn];
        ImageAndTextCell *tempCell = [[tableColumn dataCell] copy];
        
        // Calculate a base frame for new cells
        NSRect cellFrame = NSMakeRect(0, 0, [tableColumn width], [outlineView rowHeight]);
        
        // Subtract out the intercellSpacing from the width only. The rowHeight is sans-spacing
        cellFrame.size.width -= [outlineView intercellSpacing].width;
        
        [draggingInfo enumerateDraggingItemsWithOptions:0 forView:self.outlineView classes:classes searchOptions:nil usingBlock:^(NSDraggingItem *draggingItem, NSInteger index, BOOL *stop) {
            OnePDFOutlineNodeData *newNodeData = (OnePDFOutlineNodeData *)draggingItem.item;
            // Wrap the model object in a tree node
            NSTreeNode *treeNode = [NSTreeNode treeNodeWithRepresentedObject:newNodeData];
            draggingItem.draggingFrame = cellFrame;
            
            draggingItem.imageComponentsProvider = ^(void) {
                // Setup the cell with this temporary data
                id objectValue = [self outlineView:outlineView objectValueForTableColumn:tableColumn byItem:treeNode];
                [tempCell setObjectValue:objectValue];
                [self outlineView:outlineView willDisplayCell:tempCell forTableColumn:tableColumn item:treeNode];
                // Ask the table for the image components from that cell
                return (NSArray *)[tempCell draggingImageComponentsWithFrame:cellFrame inView:outlineView];
            };
        }];
    }
}


/* On Mac OS 10.5 and above, NSTableView and NSOutlineView have better contextual menu support. We now see a highlighted item for what was clicked on, and can access that item to do particular things (such as dynamically change the menu, as we do here!). Each of the contextual menus in the nib file have the delegate set to be the AppController instance. In menuNeedsUpdate, we dynamically update the menus based on the currently clicked upon row/column pair.
 */
- (void)menuNeedsUpdate:(NSMenu *)menu {
    NSInteger clickedRow = [self.outlineView clickedRow];
    id item = nil;
    OnePDFOutlineNodeData *nodeData = nil;
    BOOL clickedOnMultipleItems = NO;
    NSInteger splittablePDFcount = [self numberOfSplittablePDFItemsInCurrentSelection];
    NSInteger QLcount = [self numberOfQuickLookPreviewItemsInCurrentSelection];
    
    if (clickedRow != -1) {
        // If we clicked on a selected row, then we want to consider all rows in the selection. Otherwise, we only consider the clicked on row.
        item = [self.outlineView itemAtRow:clickedRow];
        nodeData = [item representedObject];
        clickedOnMultipleItems = [self.outlineView isRowSelected:clickedRow] && ([self.outlineView numberOfSelectedRows] > 1);
    }
    
    if (menu == self.outlineViewContextMenu) {
        [self.createBookmarkMenuItem setTitle:NSLocalizedString(@"New Bookmark", nil)];
        [self.addFilesMenuItem setTitle:NSLocalizedString(@"Add Files…", nil)];
        
        if (nodeData != nil) {
            if (clickedOnMultipleItems) {
                // We could walk through the selection and note what was clicked on at this point
                [self.renameMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Rename \"%@\"", nil), nodeData.name]];
                [self.expandMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Expand %ld items", nil), (long)[self.outlineView numberOfSelectedRows]]];
                [self.deleteMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Delete %ld items", nil), (long)[self.outlineView numberOfSelectedRows]]];
            } else {
                [self.renameMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Rename \"%@\"", nil), nodeData.name]];
                [self.expandMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Expand \"%@\"", nil), nodeData.name]];
                [self.deleteMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Delete \"%@\"", nil), nodeData.name]];
            }
            // Quick Look Menu Item
            if (QLcount > 1) {
                [self.QLMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Quick Look %ld items", nil), (long)QLcount]];
            } else if (QLcount == 1) {
                NSString *name = [[self namesOfQuickLookPreviewItemsInCurrentSelection] objectAtIndex:0];
                [self.QLMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Quick Look \"%@\"", nil), name]];
            }
            // Split PDF Menu Item
            if (splittablePDFcount > 1) {
                [self.splitMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Split %ld items", nil), (long)splittablePDFcount]];
            } else if (splittablePDFcount == 1) {
                NSString *name = [[self namesOfSplittablePDFItemsInCurrentSelection] objectAtIndex:0];
                [self.splitMenuItem setTitle:
                 [NSString stringWithFormat:NSLocalizedString(@"Split \"%@\"", nil), name]];
            }
            
            [self.createBookmarkMenuItem setHidden:NO];
            [self.addFilesMenuItem setHidden:NO];
            
            [self.menuSeperator setHidden:NO];
            
            [self.renameMenuItem setHidden:NO];
            [self.splitMenuItem setHidden:splittablePDFcount >= 1 ? NO : YES];
            [self.expandMenuItem setHidden:NO];
            [self.QLMenuItem setHidden:QLcount >= 1 ? NO : YES];
            [self.revealInFinderMenuItem setHidden:QLcount >= 1 ? NO : YES];
            [self.deleteMenuItem setHidden:NO];
            [self.deleteMenuItem setEnabled:YES];
        } else {
            [self.createBookmarkMenuItem setHidden:NO];
            [self.addFilesMenuItem setHidden:NO];
            
            [self.menuSeperator setHidden:YES];
            
            [self.renameMenuItem setHidden:YES];
            [self.splitMenuItem setHidden:YES];
            [self.expandMenuItem setHidden:YES];
            [self.QLMenuItem setHidden:YES];
            [self.revealInFinderMenuItem setHidden:YES];
            [self.deleteMenuItem setHidden:YES];
        }
        [menu setAutoenablesItems:NO];
        BOOL enable = ![(OnePDFAppDelegate*)[NSApp delegate] processing];
        [self.createBookmarkMenuItem setEnabled:enable];
        [self.addFilesMenuItem setEnabled:enable];
        
        [self.menuSeperator setEnabled:enable];
        
        [self.renameMenuItem setEnabled:enable];
        [self.splitMenuItem setEnabled:enable];
        [self.expandMenuItem setEnabled:enable];
        [self.QLMenuItem setEnabled:enable];
        [self.revealInFinderMenuItem setEnabled:enable];
        [self.deleteMenuItem setEnabled:enable];
    }
}

- (void)addNewDataToSelection:(OnePDFOutlineNodeData *)newChildData {
    if (self.outlineView.isARowBeingEdited) return;//[self.outlineView abortEditing];
    NSTreeNode *selectedNode;
    // We are inserting as a child of the last selected node. If there are none selected, insert it as a child of the treeData itself
    if ([self.outlineView clickedRow] != -1) {
        selectedNode = [self.outlineView itemAtRow:[self.outlineView clickedRow]];
    } else if ([self.outlineView selectedRow] != -1) {
        selectedNode = [self.outlineView itemAtRow:[self.outlineView selectedRow]];
    } else {
        selectedNode = self.rootTreeNode;
    }
    
    // If the selected node is a container, use its parent. We access the underlying model object to find this out.
    // In addition, keep track of where we want the child.
    NSInteger childIndex;
    NSTreeNode *parentNode;
    
    OnePDFOutlineNodeData *nodeData = [selectedNode representedObject];
    if (nodeData.directory) {
        // Since it was already a container, we insert it as the first child
        childIndex = 0;
        parentNode = selectedNode;
    } else {
        // The selected node is not a container, so we use its parent, and insert after the selected node
        parentNode = [selectedNode parentNode];
        childIndex = [[parentNode childNodes] indexOfObject:selectedNode] + 1; // + 1 means to insert after it.
    }
    
    // Use the new 10.7 API to update the tree directly in an animated fashion
    [self.outlineView beginUpdates];
    
    // Now, create a tree node for the data and insert it as a child and tell the outlineview about our new insertion
    NSTreeNode *childTreeNode = [NSTreeNode treeNodeWithRepresentedObject:newChildData];
    [[parentNode mutableChildNodes] insertObject:childTreeNode atIndex:childIndex];
    // NSOutlineView uses 'nil' as the root parent
    if (parentNode == self.rootTreeNode) {
        parentNode = nil;
    }
    [self.outlineView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:childIndex] inParent:parentNode withAnimation:NSTableViewAnimationEffectFade];
    
    [self.outlineView endUpdates];
    
    NSInteger newRow = [self.outlineView rowForItem:childTreeNode];
    if (newRow >= 0) {
        [self.outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:newRow] byExtendingSelection:NO];
        NSInteger column = 1;
        // With "full width" cells, there is no column
        if ([self useGroupRowLook] && newChildData.chapterMarker) {
            column = -1;
        }
        if ([self outlineView:self.outlineView
        shouldEditTableColumn:[[self.outlineView tableColumns] objectAtIndex:column]
                         item:childTreeNode])
            [self.outlineView editColumn:column row:newRow withEvent:nil select:YES];
    }
    [self updateStatusStringPages];
}

- (NSTreeNode*)createAndAddItemWithFileURL:(NSURL*)path calcPageNumbers:(BOOL)recalc {
    NSMutableArray *childNodeArray = [self.rootTreeNode mutableChildNodes];
    
    OnePDFOutlineNodeData *newNodeData = [[OnePDFOutlineNodeData alloc] initWithPasteboardPropertyList:[path pasteboardPropertyListForType:(id)kUTTypeURL] ofType:(id)kUTTypeURL];
    if (!newNodeData) return nil;
    // Wrap the model object in a tree node
    NSTreeNode *treeNode = [NSTreeNode treeNodeWithRepresentedObject:newNodeData];
    
    [self addDirectoryChildrenForTreeNode:treeNode];
    BOOL shouldAddToModal = [(OnePDFAppDelegate*)[NSApp delegate] isUTISupported:(__bridge CFStringRef)(newNodeData.kind)];
    
    if (UTTypeConformsTo((__bridge CFStringRef)(newNodeData.kind), kUTTypePDF)) {
        PDFDocument *doc = [[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:newNodeData.path]];
        if ([doc isLocked]) {
            if([(OnePDFAppDelegate*)[NSApp delegate] handleUnlockForDocument:doc]) newNodeData.PDF = doc;
            else shouldAddToModal = NO;
        }
    }
    if ([(OnePDFAppDelegate*)[NSApp delegate] isOfficeDoc:(__bridge CFStringRef)newNodeData.kind]) {
        [(OnePDFAppDelegate*)[NSApp delegate] officeDocumentImportAttempt];
    }
    
    [self.outlineView beginUpdates];
    // Add it to the model if we should
    shouldAddToModal ? [childNodeArray insertObject:treeNode atIndex:[childNodeArray count]] : nil;
    
    //[self.outlineView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:[childNodeArray count]] inParent:self.rootTreeNode withAnimation:NSTableViewAnimationSlideDown];
    [self.outlineView endUpdates];
    [self.outlineView reloadData];
    if (recalc) [self updateStatusStringPages];
    return shouldAddToModal ? treeNode : nil;
}

// Check for newly dropped directory, and add directory contents if so.
- (BOOL)addDirectoryChildrenForTreeNode:(NSTreeNode *)treeNode {
    OnePDFOutlineNodeData *obj = [treeNode representedObject];
    if (obj.directory && obj.path) {
        NSError *error = nil;
        NSArray *array = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[obj path] error:&error];
        array = [array sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
        if (error) {
            [(OnePDFAppDelegate*)[NSApp delegate] alertError:error];
            return NO;
        }
        for (NSUInteger i = 0; i < [array count]; i++) {
            NSString *path = [[obj path] stringByAppendingPathComponent:[array objectAtIndex:i]];
            NSString *fileUTIStr = @"";
            [[NSURL fileURLWithPath:path] getResourceValue:&fileUTIStr forKey:NSURLTypeIdentifierKey error:NULL];
            CFStringRef fileUTI = (__bridge CFStringRef)(fileUTIStr);
            if ([(OnePDFAppDelegate*)[NSApp delegate] isOfficeDoc:fileUTI]) {
                [(OnePDFAppDelegate*)[NSApp delegate] officeDocumentImportAttempt];
            }
            if ([(OnePDFAppDelegate*)[NSApp delegate] isUTISupported:fileUTI]) {
                OnePDFOutlineNodeData *newNodeData = [[OnePDFOutlineNodeData alloc] initWithPasteboardPropertyList:[[NSURL fileURLWithPath:path] pasteboardPropertyListForType:(id)kUTTypeURL] ofType:(id)kUTTypeURL];
                
                // Wrap the model object in a tree node
                NSTreeNode *childTreeNode = [NSTreeNode treeNodeWithRepresentedObject:newNodeData];
                
                newNodeData.chapterMarker = NO; // All children of root elements are not bookmarked by default.
                
                if (UTTypeConformsTo(fileUTI, kUTTypeDirectory) &&
                    !UTTypeConformsTo(fileUTI, kUTTypeBundle) &&
                    !UTTypeConformsTo(fileUTI, kUTTypePackage) &&
                    !UTTypeConformsTo(fileUTI, kUTTypeArchive)) {
                    if (![[path lastPathComponent] hasPrefix:@"_"]) {
                        newNodeData.directory = YES;
                        [self addDirectoryChildrenForTreeNode:childTreeNode]; // Recursively add subdirectory contents
                    } else continue;
                } else {
                    newNodeData.directory = NO;
                }
                
                BOOL shouldAddToModal = YES;
                if (UTTypeConformsTo(fileUTI, kUTTypePDF)) {
                    PDFDocument *doc = [[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:newNodeData.path]];
                    if ([doc isLocked]) {
                        if([(OnePDFAppDelegate*)[NSApp delegate] handleUnlockForDocument:doc]) newNodeData.PDF = doc;
                        else shouldAddToModal = NO;
                    }
                }
                // Add the new tree node to the given parent
                if (shouldAddToModal) [[treeNode mutableChildNodes] addObject:childTreeNode];
            }
        }
        return YES;
    }
    return NO;
}

- (void)clearTable {
    [[self.rootTreeNode mutableChildNodes] removeAllObjects];
    [self.outlineView reloadData];
}

#pragma mark Disabling Tooltips
- (BOOL)outlineView:(NSOutlineView *)outlineView shouldShowCellExpansionForTableColumn:(NSTableColumn *)tableColumn item:(id)item {
    return NO;
}
- (NSString *)outlineView:(NSOutlineView *)outlineView toolTipForCell:(NSCell *)cell rect:(NSRectPointer)rect tableColumn:(NSTableColumn *)tc item:(id)item mouseLocation:(NSPoint)ml {
    return nil;
}

#pragma mark Page Count

- (void)updateStatusStringPages {
    if (!self.statusStringUpdating) {
        self.statusStringUpdating = YES;
        self.statusLabel.stringValue = NSLocalizedString(@"Calculating Page Count...", nil);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_LOW, 0), ^(){
            NSInteger cnt = [self rootNodePageCount];
            
            if (self.cancelPageCountForDeletion && cnt == -1) {
                self.statusStringUpdating = NO;
                self.cancelPageCountForDeletion = NO;
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self deleteSelections:nil];
                });
                return;
            }
            
            NSString *pageOrPages = cnt == 1 ? NSLocalizedString(@"%d page", nil) : NSLocalizedString(@"%d pages", nil);
            self.statusLabel.stringValue = [NSString stringWithFormat:pageOrPages, cnt];
            self.statusStringUpdating = NO;
        });
    }
}

- (NSInteger)rootNodePageCount {
    return [self treeNodeChildrenPageCount:self.rootTreeNode];
}

- (NSInteger)treeNodeChildrenPageCount:(NSTreeNode*)node {
    if (!((OnePDFOutlineNodeData*)[node representedObject]).selected) return 0;
    NSInteger result = 0;
    NSArray *kids = [[node childNodes] copy];
    for (NSUInteger i=0; i<[kids count]; i++) {
        NSInteger kidcnt = [self treeNodeChildrenPageCount:[kids objectAtIndex:i]];
        if (kidcnt == -1) return -1;
        result += kidcnt;
        
        OnePDFOutlineNodeData *obj = [[kids objectAtIndex:i] representedObject];
        if (obj.selected && ![[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController] isUTIFilteredOut:obj.kind]) {
            NSInteger cnt = [[[kids objectAtIndex:i] representedObject] numberOfPagesWithCancellationFlag:self.cancelPageCountForDeletion];
            if (cnt == -1) return -1;
            result += cnt;
        }
    }
    return result;
}

#pragma mark - Quick Look

- (BOOL)acceptsPreviewPanelControl:(QLPreviewPanel *)panel {
    return YES;
}

- (void)beginPreviewPanelControl:(QLPreviewPanel *)panel {
    // We may the refresh the panel and/or set its delegate and data source.
    panel.delegate = self;
    panel.dataSource = self;
}

- (void)endPreviewPanelControl:(QLPreviewPanel *)panel {
    // Until the next call to -beginPreviewPanelControl: we may not
    // refresh the panel or change its delegate or data source.
    panel = nil;
}

// Quick Look panel data source

- (NSArray*)namesOfQuickLookPreviewItemsInCurrentSelection {
    NSMutableArray *names = [@[] mutableCopy];
    for (NSNumber *idx in [self currentlySelectedItems]) {
        OnePDFOutlineNodeData *obj = [[self.outlineView itemAtRow:[idx integerValue]] representedObject];
        [obj path] ? [names addObject:[obj name]] : nil;
    }
    return [names copy];
}

- (NSInteger)numberOfQuickLookPreviewItemsInCurrentSelection {
    NSInteger count = 0;
    for (NSNumber *idx in [self currentlySelectedItems]) {
        OnePDFOutlineNodeData *obj = [[self.outlineView itemAtRow:[idx integerValue]] representedObject];
        [obj path] ? ++count : 0;
    }
    return count;
}

- (NSInteger)numberOfPreviewItemsInPreviewPanel:(QLPreviewPanel *)panel {
    return [self numberOfQuickLookPreviewItemsInCurrentSelection];
}

- (id <QLPreviewItem>)previewPanel:(QLPreviewPanel *)panel previewItemAtIndex:(NSInteger)index {
    NSMutableArray* paths = [NSMutableArray arrayWithCapacity:[self numberOfPreviewItemsInPreviewPanel:panel]];
    self.currentPreviewItemDict = [[NSMutableDictionary alloc] init];
    
    if ([self isASingleRowClicked]) {
        // The clicked row exists and isn't part of the selection
        NSString *path = [[[self.outlineView itemAtRow:[self.outlineView clickedRow]] representedObject] path];
        path ? [paths addObject:[NSURL fileURLWithPath:path]] : nil;
        
        [self.currentPreviewItemDict setObject:@([self.outlineView clickedRow]) forKey:[NSURL fileURLWithPath:path]];
        
        // Select the clicked item (otherwise stuff goes wrong)
        [self.outlineView selectRowIndexes:[NSIndexSet indexSetWithIndex:[self.outlineView clickedRow]] byExtendingSelection:NO];
        
    } else {
        NSUInteger currentIndex = [[self.outlineView selectedRowIndexes] firstIndex];
        while (currentIndex != NSNotFound) {
            NSString *path = [[[self.outlineView itemAtRow:currentIndex] representedObject] path];
            
            if (path) {
                [paths addObject:[NSURL fileURLWithPath:path]];
                [self.currentPreviewItemDict setObject:@(currentIndex) forKey:[NSURL fileURLWithPath:path]];
            } else {
                
            }
            
            currentIndex = [[self.outlineView selectedRowIndexes] indexGreaterThanIndex:currentIndex];
        }
    }
    
    // Now to return the URL of the currently selected item...
    return [paths objectAtIndex:index];
}

// Quick Look panel delegate

- (BOOL)previewPanel:(QLPreviewPanel *)panel handleEvent:(NSEvent *)event {
    // redirect all key down events to the outline view
    if ([event type] == NSKeyDown) {
        [self.outlineView keyDown:event];
        return YES;
    }
    return NO;
}

// This delegate method provides the rect on screen from which the panel will zoom. If it's NSZeroRect, it'll just fade.
- (NSRect)previewPanel:(QLPreviewPanel *)panel sourceFrameOnScreenForPreviewItem:(id <QLPreviewItem>)item {
    //return NSZeroRect;
    NSRect rect = [self.outlineView frameOfCellAtColumn:1 row:[[self.currentPreviewItemDict objectForKey:item] integerValue]];
    rect.origin.y = self.outlineView.enclosingScrollView.frame.size.height - [self.outlineView.enclosingScrollView convertPoint:rect.origin fromView:self.outlineView].y;
    return [self.outlineView.window convertRectToScreen:rect];
}

- (id)previewPanel:(QLPreviewPanel *)panel transitionImageForPreviewItem:(id <QLPreviewItem>)item contentRect:(NSRect *)contentRect {
    return [[NSImage alloc] init];
}

- (IBAction)togglePreviewPanel:(id)previewPanel
{
    if ([QLPreviewPanel sharedPreviewPanelExists] && [[QLPreviewPanel sharedPreviewPanel] isVisible])
        [[QLPreviewPanel sharedPreviewPanel] orderOut:self];
    else
        [[QLPreviewPanel sharedPreviewPanel] makeKeyAndOrderFront:nil];
}

- (IBAction)openPreviewPanel:(id)previewPanel
{
    if (![QLPreviewPanel sharedPreviewPanelExists] || ![[QLPreviewPanel sharedPreviewPanel] isVisible])
        [[QLPreviewPanel sharedPreviewPanel] makeKeyAndOrderFront:nil];
}

- (IBAction)refreshPreviewPanel:(id)previewPanel {
    if ([QLPreviewPanel sharedPreviewPanelExists] && [[QLPreviewPanel sharedPreviewPanel] isVisible])
        [[QLPreviewPanel sharedPreviewPanel] reloadData];
}

#pragma mark - Splitting

- (BOOL)isSplittablePDF:(OnePDFOutlineNodeData*)node {
    if (node.kind && UTTypeConformsTo((__bridge CFStringRef)(node.kind), kUTTypePDF) && node.path) {
        PDFDocument *doc = node.PDF ? node.PDF : [[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:node.path]];
        if ([doc pageCount] > 1) return YES;
    }
    return NO;
}

- (NSArray*)namesOfSplittablePDFItemsInCurrentSelection {
    NSMutableArray *names = [@[] mutableCopy];
    for (NSNumber *idx in [self currentlySelectedItems]) {
        OnePDFOutlineNodeData *obj = [[self.outlineView itemAtRow:[idx integerValue]] representedObject];
        [self isSplittablePDF:obj] ? [names addObject:[obj name]] : nil;
    }
    return [names copy];
}

- (NSInteger)numberOfSplittablePDFItemsInCurrentSelection {
    NSInteger count = 0;
    for (NSNumber *idx in [self currentlySelectedItems]) {
        OnePDFOutlineNodeData *obj = [[self.outlineView itemAtRow:[idx integerValue]] representedObject];
        [self isSplittablePDF:obj] ? ++count : 0;
    }
    return count;
}

- (IBAction)splitTreeNodeFromMenu:(id)sender {
    NSMutableArray *selectedPDFs = [@[] mutableCopy];
    for (NSNumber *row in [self currentlySelectedItems]) {
        NSTreeNode *node = [self.outlineView itemAtRow:[row integerValue]];
        OnePDFOutlineNodeData *obj = [node representedObject];
        if ([self isSplittablePDF:obj]) [selectedPDFs addObject:node];
    }
    if ([selectedPDFs count] > 0) {
        NSUndoManager *undoman = [(OnePDFAppDelegate*)[NSApp delegate] undoManager];
        [self prepareUndoManagerWithCurrentSettings];
        if ([selectedPDFs count] == 1) {
            [undoman setActionName:[NSString stringWithFormat:
                                    NSLocalizedString(@"Split \"%@\"", nil),
                                    [(OnePDFOutlineNodeData *)[[selectedPDFs objectAtIndex:0] representedObject] name]]];
        } else if ([selectedPDFs count] > 1) {
            [undoman setActionName:[NSString stringWithFormat:
                                    NSLocalizedString(@"Split %ld items", nil),
                                    [selectedPDFs count]]];
        }
    }
    
    for (NSTreeNode *node in selectedPDFs) {
        [self splitTreeNode:node];
    }
}

- (BOOL)splitTreeNode:(NSTreeNode*)parentNode {
    OnePDFOutlineNodeData *parentNodeObj = [parentNode representedObject];
    NSString *path = [parentNodeObj path];
    if (!path) return NO;
        
    if (UTTypeConformsTo((__bridge CFStringRef)(parentNodeObj.kind), kUTTypePDF) && parentNodeObj.path) {
        PDFDocument *doc = parentNodeObj.PDF ? parentNodeObj.PDF : [[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:path]];
        if ([doc isLocked]) {
            if ([(OnePDFAppDelegate*)[NSApp delegate] handleUnlockForDocument:doc]) parentNodeObj.PDF = doc;
            else return NO;
        }
        
        if ([doc pageCount] <= 1) return NO;
        
        OnePDFOutlineNodeData *newParent = [[OnePDFOutlineNodeData alloc] initWithName:parentNodeObj.name];
        [newParent setDirectory:YES];
        //[newParent setPath:parentNodeObj.path];
        
        //self.statusLabel.stringValue = NSLocalizedString(@"Splitting PDF Document...", nil);
        
        [self.outlineView beginUpdates];
        
        NSTreeNode *newParentNode = [NSTreeNode treeNodeWithRepresentedObject:newParent];
        NSTreeNode *parentOfOldParentNode = [parentNode parentNode];
        NSTreeNode *parentOfOldParentNode_nilIfRoot = parentOfOldParentNode == self.rootTreeNode ? nil : parentOfOldParentNode;
        NSInteger parentNodeIdx = [[parentOfOldParentNode mutableChildNodes] indexOfObject:parentNode];
        [[parentOfOldParentNode mutableChildNodes] removeObjectAtIndex:parentNodeIdx];
        [self.outlineView removeItemsAtIndexes:[NSIndexSet indexSetWithIndex:parentNodeIdx] inParent:parentOfOldParentNode_nilIfRoot withAnimation:NSTableViewAnimationEffectNone];
        [[parentOfOldParentNode mutableChildNodes] insertObject:newParentNode atIndex:parentNodeIdx];
        [self.outlineView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:parentNodeIdx] inParent:parentOfOldParentNode_nilIfRoot withAnimation:NSTableViewAnimationEffectNone];
        
        for (NSInteger i = 0; i < [doc pageCount]; ++i) {
            PDFPage *page = [doc pageAtIndex:i];
            NSData *pageData = [page dataRepresentation];
            PDFDocument *pageDocument = [[PDFDocument alloc] initWithData:pageData];
            NSString *pageName = [NSString stringWithFormat:NSLocalizedString(@"Page %d", nil), i + 1];
            OnePDFOutlineNodeData *newNodeData = [[OnePDFOutlineNodeData alloc] initWithName:pageName];
            [newNodeData setPDF:pageDocument];
            [newNodeData setKind:parentNodeObj.kind];
            [newNodeData setChapterMarker:NO];
            
            // Now, create a tree node for the data and insert it as a child and tell the outlineview about our new insertion
            NSTreeNode *childTreeNode = [NSTreeNode treeNodeWithRepresentedObject:newNodeData];
            [[newParentNode mutableChildNodes] insertObject:childTreeNode atIndex:i];
            [self.outlineView insertItemsAtIndexes:[NSIndexSet indexSetWithIndex:i] inParent:newParentNode withAnimation:NSTableViewAnimationEffectNone];
        }
        [self.outlineView endUpdates];
        [self.outlineView expandItem:newParentNode];
        [self.outlineView setNeedsDisplay];
        [self updateStatusStringPages];
    }
    return YES;
}

#pragma mark - Misc

- (BOOL)isASingleRowClicked {
    return [self.outlineView clickedRow] != -1 && ![[self.outlineView selectedRowIndexes] containsIndex:[self.outlineView clickedRow]];
}

- (NSArray*)currentlySelectedItems {
    NSMutableArray *retarr = [@[] mutableCopy];
    if ([self isASingleRowClicked]) {
        // The clicked row exists and isn't part of the selection, so use it!
        [retarr addObject:@([self.outlineView clickedRow])];
    } else {
        NSUInteger currentIndex = [[self.outlineView selectedRowIndexes] firstIndex];
        while (currentIndex != NSNotFound) {
            [retarr addObject:@(currentIndex)];
            currentIndex = [[self.outlineView selectedRowIndexes] indexGreaterThanIndex:currentIndex];
        }
    }
    return [retarr copy];
}

- (IBAction)openSelectedInFinder:(id)sender {
    for (NSNumber*idx in [self currentlySelectedItems]) {
        OnePDFOutlineNodeData *node = [[self.outlineView itemAtRow:[idx integerValue]] representedObject];
        if ([node path]) [[NSWorkspace sharedWorkspace] selectFile:[node path] inFileViewerRootedAtPath:@""];
    }
}

@end

