//
//  NSCellWithoutHighlight.h
//  One PDF
//
//  Created by Simon on 25/01/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSCellWithoutHighlight : NSCell

@end
