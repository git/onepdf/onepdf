//
//  OnePDFFileFolderAdd.m
//  One PDF
//
//  Created by Simon on 25/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "OnePDFFileFolderAdd.h"

#import "OnePDFAppDelegate.h"
#import "OnePDFOutlineController.h"
#import "OnePDFMainWindowController.h"

@implementation OnePDFFileFolderAdd

- (IBAction)doOpen:(id)sender {
    [self doOpen];
}
    
- (BOOL)doOpen {
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    [openPanel setCanCreateDirectories:YES];
    [openPanel setPrompt:NSLocalizedString(@"Add", nil)];
    [openPanel setMessage:NSLocalizedString(@"Choose some files and folders...", nil)];
    [openPanel setCanChooseDirectories:YES];
    [openPanel setCanChooseFiles:YES];
    [openPanel setDelegate:self];
    [openPanel setAllowsMultipleSelection:YES];
    
    NSInteger modalResult = [openPanel runModal];
    BOOL result = (modalResult == NSOKButton);
    if (result) {
        if ([[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController] flippedToMainWindow]) {
            [[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] prepareUndoManagerWithCurrentSettings];
            [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Add Files", nil)];
        } else {
            [[[(OnePDFAppDelegate*)[NSApp delegate] undoManager] prepareWithInvocationTarget:[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController]] clearList:self];
            [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Add Files", nil)];
        }
        result = [self addItemsWithArrayOfFileURLs:[openPanel URLs]];
    }
    return result;
}

- (BOOL)panel:(id)sender shouldEnableURL:(NSURL *)url {    
    NSString *uti = nil;
    NSNumber *readable = @(YES);
    [url getResourceValue:&uti forKey:NSURLTypeIdentifierKey error:nil];
    //[url getResourceValue:&readable forKey:NSURLIsReadableKey error:nil];
    
    BOOL shouldEnable = [(OnePDFAppDelegate*)[NSApp delegate] isUTISupported:(__bridge CFStringRef)uti] && readable;
    
    return shouldEnable;
}

- (BOOL)addItemWithFileURL:(NSURL*)url calcPageNumbers:(BOOL)recalc {
    NSTreeNode *node = [[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] createAndAddItemWithFileURL:url calcPageNumbers:recalc];
    [[(OnePDFAppDelegate*)[NSApp delegate] outlineView] expandItem:node expandChildren:NO];
    return !!node;
    
}

- (BOOL)addItemsWithArrayOfFileURLs:(NSArray*)urlArray {
    BOOL result = NO;
    for (NSURL*URL in urlArray)
        result = [self addItemWithFileURL:URL calcPageNumbers:NO] || result;
    
    [[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] updateStatusStringPages];
    return result;
}

- (BOOL)addItemsWithArrayOfFilePaths:(NSArray*)pathArray {
    BOOL result = NO;
    for (NSString *path in pathArray)
        result = [self addItemWithFileURL:[NSURL fileURLWithPath:path] calcPageNumbers:NO] || result;
    
    [[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] updateStatusStringPages];
    return result;
}

@end
