//
//  insertionPointColoringTextfield.m
//  One PDF
//
//  Created by Simon on 16/07/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface InsertionPointColoringTextFieldCell : NSTextFieldCell
@end

@implementation InsertionPointColoringTextFieldCell

- (NSText*)setUpFieldEditorAttributes:(NSText *)textObj {
    NSTextView *fieldEditor = (NSTextView*)[super setUpFieldEditorAttributes:textObj];
    [fieldEditor setInsertionPointColor:[self textColor]];
    return fieldEditor;
}

@end
