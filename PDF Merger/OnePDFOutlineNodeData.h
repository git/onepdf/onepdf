/*
     File: SimpleNodeData.h
 Abstract: Represents the model object. 10.7 Updates: Implemented NSPasteboardWriting and NSPasteboardReading for easier pasteboard support.
 
  Version: 1.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
*/

#import <Foundation/Foundation.h>
#import <Cocoa/Cocoa.h>
#import <PDFKit/PDFKit.h>
#include "constants.h"

@interface OnePDFOutlineNodeData : NSObject<NSPasteboardWriting, NSPasteboardReading, NSCopying>

#pragma mark - Properties

#pragma mark Node details
/** Node name */
@property (readwrite, copy)                   NSString *name;
/** Node UTI (file type) */
@property (readwrite, copy)                   NSString *kind;
/** Node image (icon) */
@property (readwrite, strong)                 NSImage *image;
/** Node file path */
@property (readwrite, copy)                   NSString *path;

#pragma mark Node type details
/** Node is a directory */
@property (readwrite, getter=isDirectory)     BOOL directory;
/** Node is a bookmark (a type of node that doesn't point to a file) */
@property (readwrite, getter=isBookmark)      BOOL bookmark;

#pragma mark Node state details
/** Node is selected */
@property (readwrite, getter=isSelected)      BOOL selected;
/** Node is expanded in tree view */
@property (readwrite, getter=isExpanded)      BOOL expanded;
/** Node is marked as a chapter marker */
@property (readwrite, getter=isChapterMarker) BOOL chapterMarker;

#pragma mark Misc node details
/** Number of pages taken by node (first calc uses potentially slow methods, use background thread) */
@property (nonatomic, readonly)               NSInteger numberOfPages;
/** Yes, this is the wrong way. Yes, I'm lazy. No, I'm not changing it. */
- (NSInteger)numberOfPagesWithCancellationFlag:(BOOL*)flag;
/** Storage for PDF document, useful if file is encrypted */
@property (readwrite, strong)                 PDFDocument *PDF;

#pragma mark - Methods
#pragma mark Initialisers
- (id)initWithName:(NSString *)name;
+ (OnePDFOutlineNodeData *)nodeDataWithName:(NSString *)name;

#pragma mark Misc
- (NSComparisonResult)compare:(id)other;

@end

