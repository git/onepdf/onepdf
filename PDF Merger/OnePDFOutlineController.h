//
//  OnePDFTable.h
//  One PDF
//
//  Created by Simon on 17/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.

/*
 File: AppController.h
 Abstract: Application Controller object, and NSOutlineView dataSource/delegate.
 Also demonstrates 10.7 NSOutlineView insertion/deletion/move API.
 Version: 1.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
 */


#import <Cocoa/Cocoa.h>
#import <QuickLookUI/QuickLookUI.h>
#import "OnePDFOutline.h"
#import "NSTreeNode+Copying.h"

@interface OnePDFOutlineController : NSObject <NSOutlineViewDelegate, OnePDFOutlineController, QLPreviewPanelDataSource, QLPreviewPanelDelegate>

@property (strong) IBOutlet OnePDFOutline *outlineView;

// Menu
@property (strong) IBOutlet NSMenu *outlineViewContextMenu;
    @property (strong) IBOutlet NSMenuItem *createBookmarkMenuItem;
    @property (strong) IBOutlet NSMenuItem *addFilesMenuItem;
    /*-----------------------------------------------------*/ @property (strong) IBOutlet NSMenuItem *menuSeperator;
    @property (strong) IBOutlet NSMenuItem *renameMenuItem;
    @property (strong) IBOutlet NSMenuItem *splitMenuItem;
    @property (strong) IBOutlet NSMenuItem *expandMenuItem;
    @property (strong) IBOutlet NSMenuItem *QLMenuItem;
    @property (strong) IBOutlet NSMenuItem *revealInFinderMenuItem;
    @property (strong) IBOutlet NSMenuItem *deleteMenuItem;

@property (nonatomic,strong,setter = replaceRootNodeWithNode:) NSTreeNode *rootTreeNode;
- (void)prepareUndoManagerWithCurrentSettings;
- (void)restoreRootTreeNode:(NSTreeNode*)rootTreeNode selection:(NSIndexSet*)selection verticalScrollDistance:(double)vScroll prepUndoManager:(BOOL)prepUndoMan;
@property (strong) NSArray *draggedNodes;
@property (strong) NSMutableArray *iconImages;
@property (strong) NSMutableDictionary *currentPreviewItemDict;

@property (nonatomic, readonly) NSInteger pageCount;

- (IBAction)addBookmark:(id)sender;
- (NSTreeNode*)createAndAddItemWithFileURL:(NSURL*)path calcPageNumbers:(BOOL)recalc;

- (IBAction)deleteSelections:(id)sender;
- (IBAction)sortData:(id)sender;
- (IBAction)editClickedRow:(id)sender;

- (BOOL)allowDropOnDirectory;
- (BOOL)allowOnDropOnLeaf;
- (BOOL)allowBetweenDrop;
- (BOOL)useGroupRowLook;

- (void)clearTable;

- (IBAction)openSelectedInFinder:(id)sender;

@property (unsafe_unretained) IBOutlet NSTextField *statusLabel;
- (NSInteger)rootNodePageCount;
- (void)updateStatusStringPages;

@end
