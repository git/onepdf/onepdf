//
//  NSImage+Compression.m
//  One PDF
//
//  Created by Simon on 26/12/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "NSImage+Compression.h"

@implementation NSImage (Compression)

- (NSImage*)imageCompressedByFactor:(float)factor {
    @autoreleasepool {
        NSBitmapImageRep *imageRep = [[NSBitmapImageRep alloc] initWithData:[self TIFFRepresentation]];
        NSDictionary *options = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:factor] forKey:NSImageCompressionFactor];
        NSData *compressedData = [imageRep representationUsingType:NSJPEGFileType properties:options];
        return [[NSImage alloc] initWithData:compressedData];
    }
}

@end