//
//  NSButton+TextColor.h
//  One PDF
//
//  Created by Simon on 10/02/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSButton (TextColor)

- (NSColor *)textColor;
- (void)setTextColor:(NSColor *)textColor;

@end

@interface NSButtonCell (TextColor)

- (NSColor *)textColor;
- (void)setTextColor:(NSColor *)textColor;

@end