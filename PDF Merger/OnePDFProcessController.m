//
//  OnePDFProcessController.m
//  One PDF
//
//  Created by Simon on 17/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.

#import "OnePDFProcessController.h"
#import "OnePDFOutlineNodeData.h"

#pragma mark - Private Methods and Properties
@interface OnePDFProcessController()

- (PDFDocument*)processTreeNode:(NSTreeNode*)parentNode withParentOutline:(PDFOutline*)parentOutline;
- (PDFDocument*)processNodeData:(OnePDFOutlineNodeData *)nodeData;
- (PDFDocument*)pdfFromAttributedString:(NSAttributedString*)str;

// Pass this a full file path (including file name and extension)
- (NSError*)savePDFDocument:(PDFDocument*)doc withPath:(NSString*)nodePath;

- (NSString *)tmpFilePath;
- (NSURL *)tmpFileURL;

#pragma mark State
// The root outline item
@property (strong) PDFOutline *rootOutline;
// Number of documents currently being processed
@property NSInteger documentsBeingProcessed;
// Set if we have already shown the large file warning
@property BOOL largeFileWarningShown;
// File path of temporary directory
@property NSString *tempDirectory;

@end

#pragma mark -
@implementation OnePDFProcessController

#pragma mark - Initialisation

- (id)init {
    self = [super init];
    if (!self) return nil;
    
    self.shouldOverwrite = ^(NSString *filePath) {return YES;};
    self.errorBlock      = ^(NSError *error, NSString *msgTxt, NSString *descTxt) {
        if (error)
            NSLog(@"%@ (code %ld)", [error localizedDescription], [error code]);
        if (msgTxt || descTxt)
            NSLog(@"%@: %@", msgTxt ? NSLocalizedString(msgTxt, nil) : @"Error", NSLocalizedString(descTxt, nil));
    };
    self.largeFileWarningShown = NO;
    self.documentsBeingProcessed = 1;
    
    return self;
}

- (id)initWithRootTreeNode:(NSTreeNode*)rootTreeNode
                  fileName:(NSString*)fileName
          parentFolderPath:(NSString*)filePath
                 singlePDF:(BOOL)singlePDF
            chapterMarkers:(BOOL)chapterMarkers
               compression:(float)compression
        metaDataDictionary:(NSDictionary*)metaDataDict
         progressIndicator:(NSProgressIndicator*)progress
            overwriteBlock:(BOOL (^)(NSString*))overwriteBlock
                errorBlock:(void (^)(NSError*,NSString*,NSString*))errorBlock
               UTIsToExclude:(NSArray*)UTIs {
    if ((self = [super init])) {
        _rootNode         = rootTreeNode;
        _parentFolderPath = filePath;
        _fileName         = [[fileName componentsSeparatedByCharactersInSet:ONEPDF_ILLEGAL_FILE_NAME_CHARS] componentsJoinedByString:@"-"];
        _singlePDF        = singlePDF;
        _chapterMarkers   = chapterMarkers;
        _metaData         = metaDataDict;
        _compression      = compression;
        _progress         = progress? progress : nil;
        _shouldOverwrite  = overwriteBlock ? overwriteBlock : ^(NSString *filePath) {return YES;};
        _errorBlock       = errorBlock? errorBlock : ^(NSError *error, NSString *msgTxt, NSString *descTxt) {
            if (error)
                NSLog(@"%@ (code %ld)", [error localizedDescription], [error code]);
            if (msgTxt || descTxt)
                NSLog(@"%@: %@", NSLocalizedString(msgTxt, nil), NSLocalizedString(descTxt, nil));
        };
        _UTIsToExclude = UTIs;
        
        _largeFileWarningShown = NO;
        _documentsBeingProcessed = 1;
    }
    return self;
}

+ (id)processControllerWithRootTreeNode:(NSTreeNode*)rootTreeNode
                               fileName:(NSString*)fileName
                       parentFolderPath:(NSString*)filePath
                              singlePDF:(BOOL)singlePDF
                         chapterMarkers:(BOOL)chapterMarkers
                            compression:(float)compression
                     metaDataDictionary:(NSDictionary*)metaDataDict
                      progressIndicator:(NSProgressIndicator*)progress
                         overwriteBlock:(BOOL (^)(NSString*))overwriteBlock
                             errorBlock:(void (^)(NSError*,NSString*,NSString*))errorBlock
                            UTIsToExclude:(NSArray*)UTIs {
    OnePDFProcessController *instance = [[OnePDFProcessController alloc] initWithRootTreeNode:rootTreeNode
                                                                                     fileName:fileName
                                                                             parentFolderPath:filePath
                                                                                    singlePDF:singlePDF
                                                                               chapterMarkers:chapterMarkers
                                                                                  compression:compression
                                                                           metaDataDictionary:metaDataDict
                                                                            progressIndicator:progress
                                                                               overwriteBlock:overwriteBlock
                                                                                   errorBlock:errorBlock
                                                                                  UTIsToExclude:UTIs];
    return instance;
}

+ (BOOL)beginProcessWithRootTreeNode:(NSTreeNode*)rootTreeNode
                            fileName:(NSString*)fileName
                    parentFolderPath:(NSString*)filePath
                           singlePDF:(BOOL)singlePDF
                      chapterMarkers:(BOOL)chapterMarkers
                         compression:(float)compression
                  metaDataDictionary:(NSDictionary*)metaDataDict
                   progressIndicator:(NSProgressIndicator*)progress
                      overwriteBlock:(BOOL (^)(NSString*))overwriteBlock
                          errorBlock:(void (^)(NSError*,NSString*,NSString*))errorBlock
                         UTIsToExclude:(NSArray*)UTIs {
    OnePDFProcessController *instance = [[OnePDFProcessController alloc] initWithRootTreeNode:rootTreeNode
                                                                                     fileName:fileName
                                                                             parentFolderPath:filePath
                                                                                    singlePDF:singlePDF
                                                                               chapterMarkers:chapterMarkers
                                                                                  compression:compression
                                                                           metaDataDictionary:metaDataDict
                                                                            progressIndicator:progress
                                                                               overwriteBlock:overwriteBlock
                                                                                   errorBlock:errorBlock
                                                                                  UTIsToExclude:UTIs];
    
    return [instance beginProcess];
}

#pragma mark - Processing

- (BOOL)beginProcess {
    __block BOOL success = NO;
    __block void (^finish)(void) = ^(){
        self.isRunning = NO;
        self.largeFileWarningShown = NO;
        self.documentsBeingProcessed = 1;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:ONEPDF_PROCESS_CONTROLLER_COMPLETE_NOTIFICATION
                                                            object:self
                                                          userInfo:@{@"success":@(success)}];
    };
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        dispatch_queue_t q = dispatch_get_current_queue();
        self.rootOutline = [[PDFOutline alloc] init];
        if (self.rootNode && self.parentFolderPath && self.fileName && self.metaData) {
            self.isRunning = YES;
            NSArray *rootNodeChildNodes = [self.rootNode childNodes];
            NSInteger rootNodeAllNodesCount = [self treeNodeChildrenCount:self.rootNode];
            NSInteger pageCount = [self treeNodeChildrenPageCount:self.rootNode];
            if (pageCount<1) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    self.errorBlock(nil, @"List is empty", @"Please add some files to the list");
                });
                finish();
                return;
            }
            if (rootNodeAllNodesCount > ONEPDF_LARGE_FILE_AMOUNT) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    self.errorBlock(nil, @"Large amount of file(s) encountered", @"Processing may appear to stall, please be patient!");
                });
            }
            dispatch_sync(q, ^(){
                [self.progress setDoubleValue:0];
                [self.progress setMaxValue:pageCount*2];
                [self.progress startAnimation:self];
            });
            
            dispatch_sync(q, ^(){
                if (self.singlePDF) {
                    NSString *nodePath = [NSString stringWithFormat:@"%@/%@.pdf",self.parentFolderPath,self.fileName];
                    PDFDocument *doc = nil;
                    @autoreleasepool {
                        doc = [self processTreeNode:self.rootNode withParentOutline:nil];
                        NSError *saved = [self savePDFDocument:doc withPath:nodePath];
                        success = success && !!saved;
                        if (!!saved) {
                            dispatch_async(dispatch_get_main_queue(), ^(){
                                self.errorBlock(saved, nil, nil);
                            });
                        }
                    }
                    doc = nil; // Just in case...
                } else {
                    self.documentsBeingProcessed = [rootNodeChildNodes count];
                    for (NSTreeNode *node in rootNodeChildNodes) {
                        OnePDFOutlineNodeData *objData = [node representedObject];
                        NSString *nodePath = [NSString stringWithFormat:@"%@/%@/%@.pdf",
                                              self.parentFolderPath,
                                              self.fileName,
                                              objData.name];
                        
                        PDFDocument *doc = nil;
                        @autoreleasepool {
                            doc = [self processTreeNode:node withParentOutline:nil];
                            NSError *saved = [self savePDFDocument:doc withPath:nodePath];
                            success = success && !!saved;
                            if (!!saved) {
                                dispatch_async(dispatch_get_main_queue(), ^(){
                                    self.errorBlock(saved, nil, nil);
                                });
                            }
                        }
                        doc = nil; // Just in case...
                    }
                }
            });
            dispatch_barrier_sync(q, ^{
                [self.progress setDoubleValue:[self.progress maxValue]];
                [self.progress stopAnimation:self];
                finish();
            });
        }
    });
    return success;
}

- (PDFDocument*)processTreeNode:(NSTreeNode*)parentNode withParentOutline:(PDFOutline*)parentOutline {
    PDFDocument *pdf = [[PDFDocument alloc] init];
    parentOutline = parentOutline ? parentOutline : self.rootOutline; // if there is no parent, it's root
    
    NSMutableArray *bookmarksToBeAdded = [@[] mutableCopy];
    for (NSTreeNode *obj in [parentNode childNodes]) {
        BOOL nodeIsBookmark = NO;
        
        OnePDFOutlineNodeData *objData = [obj representedObject];
        PDFDocument *newPDF;
        PDFOutline *bookmark = [[PDFOutline alloc] init];
        PDFDestination *dest;
        BOOL shouldCountPages = YES;
        
        BOOL shouldBeExcluded = NO;
        for (NSString *uti in self.UTIsToExclude)
            if (UTTypeConformsTo((__bridge CFStringRef)(objData.kind), (__bridge CFStringRef)(uti)))
                shouldBeExcluded = YES;
        
        if (shouldBeExcluded) continue;
        
        if (objData.directory) {
            // It's a directory, so use recursion.
            if (objData.selected) {
                if (objData.chapterMarker) {
                    [bookmark setLabel:objData.name];
                    newPDF = [self processTreeNode:obj withParentOutline:bookmark];
                } else {
                    newPDF = [self processTreeNode:obj withParentOutline:parentOutline];
                }
                shouldCountPages = NO;
            }
        } else if (objData.bookmark) {
            // It's a bookmark, add its name to the array of names to point to the top of the next file...
            if (objData.selected) {
                [bookmarksToBeAdded addObject:objData.name];
                nodeIsBookmark = YES;
            }
        } else {
            // It's a file, process it
            if (objData.selected) {
                newPDF = [self processNodeData:objData];
                BOOL outlineExists = newPDF.outlineRoot.numberOfChildren > 0;
                if (objData.chapterMarker) {
                    if (outlineExists) bookmark = newPDF.outlineRoot;
                    [bookmark setLabel:[[objData name] stringByDeletingPathExtension]];
                }
            }
        }
        if (self.chapterMarkers && !nodeIsBookmark) {
            // Top left point of first page of the currently processing document
            PDFPage *currentPage = [newPDF pageAtIndex:0];
            CGFloat currentPageHeight = [currentPage boundsForBox:kPDFDisplayBoxMediaBox].size.height;
            dest = [[PDFDestination alloc] initWithPage:currentPage atPoint:NSMakePoint(0,currentPageHeight)];
            // If any bookmarks need to be added, add them and then remove them all from the array
            for (NSString*bookmark in bookmarksToBeAdded) {
                PDFOutline *lastBookmark = [[PDFOutline alloc] init];
                [lastBookmark setDestination:dest];
                [lastBookmark setLabel:bookmark];
                [parentOutline insertChild:lastBookmark atIndex:[parentOutline numberOfChildren]];
            }
            [bookmarksToBeAdded removeAllObjects];
            
            // If this node is a chapter marker itself, add that as well...
            if (![[bookmark label] isEqualToString:@""]) {
                [bookmark setDestination:dest];
                [parentOutline insertChild:bookmark atIndex:[parentOutline numberOfChildren]];
            }
        }
        // Iterate through the new PDF file, and add each page to the main PDF file.
        if (newPDF) {
            for (NSUInteger i=0; i<[newPDF pageCount]; i++) {
                [pdf insertPage:[newPDF pageAtIndex:i] atIndex:[pdf pageCount]];
                if (shouldCountPages) [self.progress incrementBy:1];
            }
        }
        
        // Nullification of certain memory-suckers... (ARC doesn't release quickly enough for my taste)
        newPDF = nil;
    }
    
    if ([bookmarksToBeAdded count]>0) {
        // Top left point of last page of entire document
        PDFPage *currentPage = [pdf pageAtIndex:[pdf pageCount]-1];
        CGFloat currentPageHeight = [currentPage boundsForBox:kPDFDisplayBoxMediaBox].size.height;
        PDFDestination *dest = [[PDFDestination alloc] initWithPage:currentPage atPoint:NSMakePoint(0,currentPageHeight)];
        
        // If any bookmarks need to be added, add them and then remove them all from the array
        for (NSString*bookmark in bookmarksToBeAdded) {
            PDFOutline *lastBookmark = [[PDFOutline alloc] init];
            [lastBookmark setDestination:dest];
            [lastBookmark setLabel:bookmark];
            [parentOutline insertChild:lastBookmark atIndex:[parentOutline numberOfChildren]];
        }
        [bookmarksToBeAdded removeAllObjects];
    }
    
    return pdf;
}

- (PDFDocument*)processNodeData:(OnePDFOutlineNodeData *)nodeData {
    PDFDocument *pdf = [[PDFDocument alloc] init];
    CFStringRef fileUTI = (CFStringRef)CFBridgingRetain([nodeData kind]);
    if (nodeData.path || (UTTypeConformsTo(fileUTI, kUTTypePDF) && nodeData.PDF)) {
        if([[NSFileManager defaultManager] fileExistsAtPath:nodeData.path] || (UTTypeConformsTo(fileUTI, kUTTypePDF) && nodeData.PDF)) {
            if (UTTypeConformsTo(fileUTI, kUTTypeImage)) {
                NSImage *image = [[NSImage alloc] initWithContentsOfFile:nodeData.path];
                if (image) {
                    if (self.compression < 1 && self.compression >= 0) image = [image imageCompressedByFactor:self.compression];
                    PDFPage *page = [[PDFPage alloc] initWithImage:image];
                    [pdf insertPage:page atIndex:[pdf pageCount]];
                } else {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        self.errorBlock(nil, @"Error", [NSString stringWithFormat:NSLocalizedString(@"File at \"%@\" is not readable", nil), nodeData.path]);
                    });
                }
            } else if (UTTypeConformsTo(fileUTI, kUTTypePDF)) {
                pdf = nodeData.PDF ? nodeData.PDF : [[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:nodeData.path]];
                if (pdf==nil) {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        self.errorBlock(nil, @"Error", [NSString stringWithFormat:NSLocalizedString(@"File at \"%@\" is not readable", nil), nodeData.path]);
                    });
                }
            } else if (UTTypeConformsTo(fileUTI, kUTTypeText) || UTTypeConformsTo(fileUTI, kUTTypeCompositeContent)) {
                NSAttributedString *content = [[NSAttributedString alloc] initWithPath:nodeData.path documentAttributes:nil];
                pdf = [self pdfFromAttributedString:content];
                if (pdf==nil) {
                    dispatch_async(dispatch_get_main_queue(), ^(){
                        self.errorBlock(nil, @"Error", [NSString stringWithFormat:NSLocalizedString(@"File at \"%@\" could not be processed", nil), nodeData.path]);
                    });
                }
            } else {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    self.errorBlock(nil, @"Error", [NSString stringWithFormat:NSLocalizedString(@"File at \"%@\" is an unsupported file type", nil), nodeData.path]);
                });
            }
        } else {
            dispatch_async(dispatch_get_main_queue(), ^(){
                self.errorBlock(nil, @"Error", [NSString stringWithFormat:NSLocalizedString(@"File at \"%@\" does not exist", nil), nodeData.path]);
            });
        }
    } else {
        dispatch_async(dispatch_get_main_queue(), ^(){
            self.errorBlock(nil, @"Internal Error", [NSString stringWithFormat:NSLocalizedString(@"Internal error at file \"%@\"", nil), nodeData.path]);
        });
    }
    CFRelease(fileUTI);
    return pdf;
}

- (PDFDocument*)pdfFromAttributedString:(NSAttributedString*)str {
    if (![str isEqualToAttributedString:[[NSAttributedString alloc] init]]) {
        if (1) {
            // Allocate a textview
            NSTextView *txtView = [[NSTextView alloc] initWithFrame:ONEPDF_PAPER_SIZE]; // Approx A4 size
            [[txtView textStorage] setAttributedString:str];
            
            // Get a temporary file path
            NSURL *tmpURL = [self tmpFileURL];
            if (!tmpURL) return nil;
            
            // Settings for print operation
            NSPrintInfo *sharedInfo = [NSPrintInfo sharedPrintInfo];
            NSMutableDictionary *printInfoDict = [NSMutableDictionary dictionaryWithDictionary:[sharedInfo dictionary]];
            [printInfoDict setObject:NSPrintSaveJob forKey:NSPrintJobDisposition];
            [printInfoDict setObject:tmpURL forKey:NSPrintJobSavingURL];
            NSPrintInfo *printInfo = [[NSPrintInfo alloc] initWithDictionary:printInfoDict];
            [printInfo setHorizontalPagination:NSFitPagination];
            [printInfo setVerticalPagination:NSAutoPagination];
            [printInfo setVerticallyCentered:NO];
            
            // Print the textview to a PDF at the temporary file URL we got earlier
            NSPrintOperation *printOp = [NSPrintOperation printOperationWithView:txtView printInfo:printInfo];
            [printOp setShowsPrintPanel:NO];
            [printOp setShowsProgressPanel:NO];
            [printOp runOperation];
            
            // Load the file
            PDFDocument *pdf = [[PDFDocument alloc] initWithURL:tmpURL];
            // Delete it
            [[NSFileManager defaultManager] removeItemAtURL:tmpURL error:nil];
            return pdf;
            
        } else { // This version works, but doesn't support pagination.
            // Allocate a textview
            NSTextView *txtView = [[NSTextView alloc] initWithFrame:ONEPDF_PAPER_SIZE]; // Approx A4 size
            [[txtView textStorage] setAttributedString:str];
            // NSLayoutManager is lazy
            [[txtView layoutManager] glyphRangeForTextContainer:[txtView textContainer]];
            
            NSPrintInfo *printInfo = [NSPrintInfo sharedPrintInfo];
            [printInfo setHorizontalPagination:NSFitPagination];
            [printInfo setVerticalPagination:NSAutoPagination];
            [printInfo setVerticallyCentered:NO];
            
            NSMutableData *dat = [NSMutableData data];
            //NSLog(@"%@",NSStringFromRect([[txtView layoutManager] usedRectForTextContainer:[txtView textContainer]]));
            NSPrintOperation *printOp = [NSPrintOperation PDFOperationWithView:txtView
                                                                    insideRect:[[txtView layoutManager] usedRectForTextContainer:[txtView textContainer]]
                                                                        toData:dat
                                                                     printInfo:printInfo];
            [printOp setShowsPrintPanel:NO];
            [printOp setShowsProgressPanel:NO];
            [printOp runOperation];
            
            PDFDocument *pdf = [[PDFDocument alloc] initWithData:dat];
            return pdf;
        }
    } else return nil;
}

#pragma mark - Writing PDFDocument to file

- (NSError*)savePDFDocument:(PDFDocument*)doc withPath:(NSString*)nodePath {
    self.rootOutline ? [doc setOutlineRoot:self.rootOutline] : nil;
    
    NSError *error = nil;
    if (!self.singlePDF)
        [[NSFileManager defaultManager] createDirectoryAtPath:[nodePath stringByDeletingLastPathComponent]
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:&error];
    else
        [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[nodePath stringByDeletingLastPathComponent] error:&error];
    if (!!error) return error;
    
    if (self.metaData) [doc setDocumentAttributes:self.metaData];
    
    // If file already exists, add number to end
    __block NSString *currPath = nodePath;
    __block BOOL willOverwrite = YES;
    if ([[NSFileManager defaultManager] fileExistsAtPath:currPath]) {
        // Just in case it's an NSAlert or something
        // else that shouldn't be run on a background
        // thread, we are running on the main thread.
        dispatch_sync(dispatch_get_main_queue(), ^(){
                if (self.shouldOverwrite != nil)
                    willOverwrite = self.shouldOverwrite(currPath);
                else willOverwrite = NO;
        });
    }
    if (!willOverwrite) {
        for (NSUInteger i = 1; [[NSFileManager defaultManager] fileExistsAtPath:currPath] && (i>0 && i<10000); i++)
            currPath = [NSString stringWithFormat:@"%@ %ld.%@",[nodePath stringByDeletingPathExtension],(long)i,[nodePath pathExtension]];
    }
    
    if (![[NSFileManager defaultManager] createFileAtPath:currPath contents:[NSData data] attributes:nil]) {
        error = [NSError errorWithDomain:NSPOSIXErrorDomain code:errno userInfo:nil];
        return error;
    }
    
    dispatch_sync(dispatch_get_current_queue(), ^(){
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(updateWriteProgress:)
                                                     name:PDFDocumentDidEndPageWriteNotification
                                                   object:doc];
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(writeOperationDone:)
                                                     name:PDFDocumentDidEndWriteNotification
                                                   object:doc];
        [doc writeToFile:currPath];
    });
    
    return error;
}

- (void)updateWriteProgress:(NSNotification *)notification {
    [self.progress incrementBy:1.0 / self.documentsBeingProcessed];
}

- (void)writeOperationDone:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Utility Methods

- (NSString *)tmpFilePath {
    // Construct a temporary file path (and create any directories leading up to it)
    if (!self.tempDirectory) {
        NSString *tempFileDirectory = [NSTemporaryDirectory() stringByAppendingPathComponent:[[NSBundle mainBundle] bundleIdentifier]];
        if (![[NSFileManager defaultManager] fileExistsAtPath:tempFileDirectory]) {
            NSError *error = nil;
            [[NSFileManager defaultManager] createDirectoryAtPath:tempFileDirectory
                                      withIntermediateDirectories:YES
                                                       attributes:nil
                                                            error:&error];
            if (error) {
                dispatch_async(dispatch_get_main_queue(), ^(){
                    self.errorBlock(nil, @"Internal Error", @"Temporary file creation failed");
                });
                return nil;
            }
        }
        self.tempDirectory = tempFileDirectory;
    }
    NSString *tempFileName = [self.tempDirectory stringByAppendingPathComponent:[[NSProcessInfo processInfo] globallyUniqueString]];
    
    return tempFileName;
}

- (NSURL*)tmpFileURL {
    NSString *path = [self tmpFilePath];
    return path ? [NSURL fileURLWithPath:path] : nil;
}

- (void)setFileName:(NSString *)fileName {
    _fileName = [[_fileName componentsSeparatedByCharactersInSet:ONEPDF_ILLEGAL_FILE_NAME_CHARS] componentsJoinedByString:@"-"];
}

- (NSInteger)treeNodeChildrenCount:(NSTreeNode*)node {
    NSInteger result = 0;
    NSArray *kids = [node childNodes];
    for (NSUInteger i = 0; i < [kids count]; i++, result++) {
        result += [self treeNodeChildrenCount:[kids objectAtIndex:i]];
    }
    return result;
}

- (NSInteger)treeNodeChildrenPageCount:(NSTreeNode*)node {
    NSInteger result = 0;
    NSArray *kids = [node childNodes];
    for (NSUInteger i = 0; i < [kids count]; result += [self nodePageCount:[kids objectAtIndex:i]], i++) {
        result += [self treeNodeChildrenPageCount:[kids objectAtIndex:i]];
    }
    return result;
}

- (NSInteger)nodePageCount:(NSTreeNode*)node {
    OnePDFOutlineNodeData *nodeData = [node representedObject];
    BOOL shouldBeExcluded = NO;
    for (NSString *uti in self.UTIsToExclude)
        if (UTTypeConformsTo((__bridge CFStringRef)(nodeData.kind), (__bridge CFStringRef)(uti)))
            shouldBeExcluded = YES;
    if(([[NSFileManager defaultManager] fileExistsAtPath:nodeData.path] || nodeData.PDF) && !shouldBeExcluded) {
        NSInteger cnt = nodeData.numberOfPages;
        
        NSDictionary *attrs = [[NSFileManager defaultManager] attributesOfItemAtPath:nodeData.path error:nil];
        NSInteger fileSize = [attrs fileSize];
        
        if ((cnt > ONEPDF_LARGE_PAGE_AMOUNT || fileSize > ONEPDF_LARGE_FILE_SIZE) && !self.largeFileWarningShown) {
            dispatch_async(dispatch_get_main_queue(), ^(){
                self.errorBlock(nil, @"Large file(s) encountered", @"Processing may appear to stall, please be patient!");
                self.largeFileWarningShown = YES;
            });
        }
        return cnt;
    } else return 0;
}

@end