//
//  OnePDFMainWindowController.m
//  One PDF
//
//  Created by Simon on 17/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "OnePDFMainWindowController.h"

#import "OnePDFAppDelegate.h"
#import "OnePDFOutline.h"
#import "OnePDFOutlineController.h"
#import "OnePDFFileFolderAdd.h"

#import "OnePDFProcessController.h"

#import "OnePDFCustomDrawerPanel.h"

#import "NSWindow+Flipping.h"
#import "NSButton+TextColor.h"

@interface OnePDFMainWindowController ()

- (void)morphToMainWindowWithRootNode:(NSTreeNode*)rootNode outlineSelection:(NSIndexSet*)sel verticalScrollDistance:(double)vScroll;

@property int overwriteAll; // 0 if "No to all", 1 if "Yes to all", anything else if no selection



@property NSRect popoverFrame;

@end

@implementation OnePDFMainWindowController

- (void)awakeFromNib {
    self.flippedToMainWindow = NO;
    [self.progress setHidden:YES];
    
    // Window starting positions
    [self.dragWindow center];
    [self.mainWindow center];
    dragWindowStartingSize = self.dragWindow.frame.size;
    mainWindowStartingSize = self.mainWindow.frame.size;
    [self.dragWindow makeKeyAndOrderFront:self];
    
    // Localize things...
    [self.dragWindow setTitle:NSLocalizedString([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"], nil)];
    [self.mainWindow setTitle:NSLocalizedString([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"], nil)];
    
    // Setup cell text to be WHITE, DAMMIT
    [self.singlePDFOption setTextColor:[NSColor whiteColor]];
    [self.singlePDFOption setHighlightsBy:NSNoCellMask];
    [self.multiplePDFOption setTextColor:[NSColor whiteColor]];
    [self.multiplePDFOption setHighlightsBy:NSNoCellMask];
    [self.chapterMarkers setTextColor:[NSColor whiteColor]];
    [self.chapterMarkers setHighlightsBy:NSNoCellMask];
    
    for (NSCell *currentCell in [self.filterMatrix cells]) {
        if ([currentCell respondsToSelector:@selector(setTextColor:)])
            [(NSButtonCell*)currentCell setTextColor:[NSColor whiteColor]];
        if ([currentCell respondsToSelector:@selector(setHighlightsBy:)])
            [(NSButtonCell*)currentCell setHighlightsBy:NSNoCellMask];
    }
    
    // Setup popover, use black styling
    INPopoverController *mdpc = [[INPopoverController alloc] initWithContentViewController:self.metadataViewController];
    mdpc.color = [NSColor colorWithDeviceWhite:0.180 alpha:0.98];
	mdpc.borderColor = [NSColor colorWithCalibratedWhite:0.0 alpha:0.8];
	mdpc.highlightBorderColor = [NSColor colorWithDeviceWhite:1.0 alpha:0.1];
	mdpc.borderWidth = 1.0;
	mdpc.cornerRadius = 5;
	mdpc.closesWhenEscapeKeyPressed = YES;
	mdpc.closesWhenPopoverResignsKey = NO;
	mdpc.closesWhenApplicationBecomesInactive = NO;
	mdpc.animates = YES;
	mdpc.animationType = INPopoverAnimationTypePop;
    [mdpc setInitialFirstResponder:self.titleMeta];
    [mdpc setDelegate:self];
    self.metadataPopoverController = mdpc;
    self.popoverFrame = NSZeroRect;
    
    // Register to know when the outline is empty
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allElementsRemovedFromTree)
                                                 name:ONEPDF_OUTLINE_EMPTY_NOTIFICATION
                                               object:[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController]];
}

- (IBAction)morphToMainWindow:(id)sender {
    if (self.flippedToMainWindow) return;
    
    NSRect dragWindowFrame = self.dragWindow.frame;
    NSRect newDragWindowFrame = NSMakeRect(dragWindowFrame.origin.x,
                                           dragWindowFrame.origin.y,
                                           dragWindowStartingSize.width,
                                           dragWindowStartingSize.height);
    
    [self.mainWindow center];
    
    [self.mainWindow setMovableByWindowBackground:NO];
    [self.dragWindow setMovableByWindowBackground:NO];
    
    [self.dragWindow setFrame:self.mainWindow.frame display:YES animate:YES];
    [self.dragWindow flipToShowWindow:self.mainWindow forward:YES];
    
    [self.customDrawer setAlphaValue:0];
    [self.mainWindow addChildWindow:self.customDrawer ordered:NSWindowBelow];
    [self animateCustomDrawerToRightOfMainWindow];
    
    [self.mainWindow makeFirstResponder:[(OnePDFAppDelegate*)[NSApp delegate] outlineView]];
    
    [self.mainWindow setMovableByWindowBackground:YES];
    [self.dragWindow setMovableByWindowBackground:YES];
    
    [self.dragWindow setFrame:newDragWindowFrame display:YES];
    self.flippedToMainWindow = YES;
}

- (void)morphToDragWindow {
    if (self.currentProcessControllerIsRunning || !self.flippedToMainWindow) return;
    
    NSRect mainWindowFrame = self.mainWindow.frame;
    NSRect newMainWindowFrame = NSMakeRect(mainWindowFrame.origin.x,
                                           mainWindowFrame.origin.y,
                                           mainWindowStartingSize.width,
                                           mainWindowStartingSize.height);
    [self.dragWindow center];
    
    [self.mainWindow setMovableByWindowBackground:NO];
    [self.dragWindow setMovableByWindowBackground:NO];
    
    self.customDrawer.isOpen?[self.customDrawer toggleWithAnimation:YES]:nil;
    [self animateCustomDrawerBehindMainWindow];
    [self.mainWindow removeChildWindow:self.customDrawer];
    [self.customDrawer orderOut:self];
    [self.mainWindow setFrame:self.dragWindow.frame display:YES animate:YES];
    [self.mainWindow flipToShowWindow:self.dragWindow forward:NO];
    
    [self.mainWindow setMovableByWindowBackground:YES];
    [self.dragWindow setMovableByWindowBackground:YES];
    
    [self.mainWindow setFrame:newMainWindowFrame display:YES];
    
    [[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] clearTable];
    self.flippedToMainWindow = NO;
}

- (IBAction)clearList:(id)sender {
    if (self.currentProcessControllerIsRunning) return;
    
    [[[(OnePDFAppDelegate*)[NSApp delegate] undoManager] prepareWithInvocationTarget:self]
     morphToMainWindowWithRootNode:[[[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] rootTreeNode] copy]
     outlineSelection:[[(OnePDFAppDelegate*)[NSApp delegate] outlineView] selectedRowIndexes]
     verticalScrollDistance:[[[[(OnePDFAppDelegate*)[NSApp delegate] outlineView] enclosingScrollView] verticalScroller] doubleValue]];
    if ([[(OnePDFAppDelegate*)[NSApp delegate] outlineView] isARowBeingEdited]) [self.mainWindow makeFirstResponder:nil];
    [self morphToDragWindow];
}

- (void)morphToMainWindowWithRootNode:(NSTreeNode*)rootNode outlineSelection:(NSIndexSet*)sel verticalScrollDistance:(double)vScroll {
    [[[(OnePDFAppDelegate*)[NSApp delegate] undoManager] prepareWithInvocationTarget:self] clearList:self];
    
    [[(OnePDFAppDelegate*)(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] restoreRootTreeNode:rootNode selection:sel verticalScrollDistance:vScroll prepUndoManager:NO];
    if (!self.flippedToMainWindow)
        [self morphToMainWindow:self];
}

#pragma mark - Button actions

- (IBAction)openButtonAction:(id)sender { // For the main "Open" button in dragWindow
    if (self.currentProcessControllerIsRunning) return;
    
    if ([[(OnePDFAppDelegate*)[NSApp delegate] fileFolderAdd] doOpen]) {
        [self performSelector:@selector(morphToMainWindow:) withObject:self afterDelay:0.25];
    }
}

#pragma mark Toolbar Buttons

- (IBAction)clearButtonAction:(id)sender {
    if (self.currentProcessControllerIsRunning) return;
    
    [self clearList:self];
    [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Clear List", nil)];
}

- (IBAction)addButtonAction:(id)sender {
    if (self.currentProcessControllerIsRunning) return;
    
    [[(OnePDFAppDelegate*)[NSApp delegate] fileFolderAdd] doOpen];
}

- (IBAction)processButtonAction:(id)sender {
    if (self.currentProcessControllerIsRunning) return;
    
    [self beginProcess];
}

#pragma mark - Window Delegate Functions

- (void)windowDidResize:(NSNotification *)notification {
    if (notification.object==self.mainWindow) {
        [self positionCustomDrawerToRightOfMainWindow];
        mainWindowZoomed = NO;
    }
}

- (void)animateCustomDrawerToRightOfMainWindow {
    [self.customDrawer setAlphaValue:0];
    [self positionCustomDrawerBehindMainWindow];
    NSRect newFrame = self.customDrawer.frame;
    newFrame = CGRectOffset(newFrame, self.customDrawer.rightSideWidth, 0);
    [self.customDrawer setAlphaValue:1];
    [self.customDrawer setFrame:newFrame display:YES animate:YES];
}

- (void)animateCustomDrawerBehindMainWindow {
    [self positionCustomDrawerToRightOfMainWindow];
    NSRect newFrame = self.customDrawer.frame;
    newFrame = CGRectOffset(newFrame, -(self.customDrawer.rightSideWidth), 0);
    [self.customDrawer setFrame:newFrame display:YES animate:YES];
}

- (void)positionCustomDrawerBehindMainWindow {
    [self positionCustomDrawerToRightOfMainWindow];
    NSRect newFrame = self.customDrawer.frame;
    newFrame = CGRectOffset(newFrame, -(self.customDrawer.rightSideWidth), 0);
    [self.customDrawer setFrame:newFrame display:YES animate:NO];
}

- (void)positionCustomDrawerToRightOfMainWindow {
    [self.customDrawer positionToRightOfWindow:self.mainWindow withHeight:[[(OnePDFAppDelegate*)[NSApp delegate] outlineView] superview].frame.size.height + 2];
}

- (BOOL)windowShouldZoom:(NSWindow *)sender toFrame:(NSRect)newFrame {
    if (sender==self.mainWindow) {
        if (mainWindowZoomed) {
            mainWindowZoomed = NO;
            [sender setFrame:mainWindowPrevFrame
                     display:YES
                     animate:YES];
        }
        else {
            mainWindowPrevFrame = sender.frame;
            [sender setFrame:NSMakeRect(newFrame.origin.x,
                                        newFrame.origin.y,
                                        self.customDrawer.isOpen?
                                        newFrame.size.width-self.customDrawer.frame.size.width:
                                        newFrame.size.width-self.customDrawer.rightSideWidth,
                                        newFrame.size.height
                                        )
                     display:YES
                     animate:YES];
            mainWindowZoomed = YES;
        }
        return NO;
    } else return YES;
}

- (BOOL)windowShouldClose:(NSWindow *)sender {
    return [(OnePDFAppDelegate*)[NSApp delegate] quitApp];
}

- (NSUndoManager*)undoManager {
    return [(OnePDFAppDelegate*)(OnePDFAppDelegate*)[NSApp delegate] undoManager];
}

- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window {
    return self.undoManager;
}

#pragma mark Popover Functions

- (IBAction)toggleMetadataPopover:(id)sender {
    if (self.metadataPopoverController.popoverIsVisible) {
        [self hideMetadataPopover:sender];
    } else {
        [self showMetadataPopover:sender];
    }
}

- (IBAction)showMetadataPopover:(id)sender {
    if (!CGRectEqualToRect(self.popoverFrame, NSZeroRect)) [self.metadataPopoverController setContentSize:self.popoverFrame.size];
    [self.metadataPopoverController presentPopoverFromRect:[sender bounds] inView:sender preferredArrowDirection:INPopoverArrowDirectionUp anchorsToPositionView:YES];
    //[self.mainWindow makeFirstResponder:self.titleMeta];
}

- (IBAction)hideMetadataPopover:(id)sender {
    self.popoverFrame = [[self.metadataViewController view] frame];
    [self.metadataPopoverController closePopover:nil];
}

#pragma mark Methods for controls...

- (void)pathControl:(NSPathControl *)pathControl willPopUpMenu:(NSMenu *)menu {
    // We don't want to show the useless "parent folders" menu items, as they are very confusing.
    while ([[menu itemArray] count] > 3) {
        [menu removeItemAtIndex:3];
    }
}

- (void)pathControl:(NSPathControl *)pathControl willDisplayOpenPanel:(NSOpenPanel *)openPanel {
    [openPanel setDelegate:self];
}

- (BOOL)panel:(id)sender shouldEnableURL:(NSURL *)url {
    NSNumber *directory = @(YES);
    [url getResourceValue:&directory forKey:NSURLIsDirectoryKey error:nil];
    return [directory boolValue];
}

- (NSDragOperation)pathControl:(NSPathControl *)pathControl validateDrop:(id <NSDraggingInfo>)info {
    NSPasteboard *pasteboard = [info draggingPasteboard];
    
    NSString *theType = [pasteboard availableTypeFromArray:[NSArray arrayWithObject:NSURLPboardType]];
    
    if ([theType isEqualToString:NSURLPboardType]) {
        NSURL *url = [NSURL URLWithString:[[pasteboard propertyListForType:NSURLPboardType] objectAtIndex:0]];
        NSNumber *writable = @(YES);
        NSNumber *directory = @(YES);
        [url getResourceValue:&writable forKey:NSURLIsWritableKey error:nil];
        [url getResourceValue:&directory forKey:NSURLIsWritableKey error:nil];
        return [writable boolValue] && [directory boolValue] ? NSDragOperationGeneric : NSDragOperationNone;
    } else return NSDragOperationNone;
}

- (IBAction)compressionUpdate:(id)sender {
    if ([self.compression.stringValue isEqualToString:@"1"])
        [self.compressionLabel setStringValue:NSLocalizedString(@"None", nil)];
    else
        [self.compressionLabel setStringValue:[self.compression stringValue]];
}

- (void)controlTextDidChange:(NSNotification*)notification {
    NSTextField *changedField = [notification object];
    if ([changedField isEqualTo:[self.fileNameTxt controlView]]) {
        // Update fileNameTxt every time it changes...
        [self.fileNameTxt setStringValue:[changedField stringValue]];
    }
}

#pragma mark Go!

- (void)beginProcess {
    if (self.currentProcessControllerIsRunning) return;
    
    // Ask for output dir if not selected
    if ([self.pathPopUp URL] == nil) {
        NSOpenPanel *exportdir = [NSOpenPanel openPanel];
        [exportdir setCanCreateDirectories:YES];
        [exportdir setTitle:NSLocalizedString(@"Choose output directory", nil)];
        [exportdir setPrompt:NSLocalizedString(@"Choose", nil)];
        [exportdir setCanChooseDirectories:YES];
        [exportdir setCanChooseFiles:NO];
        [exportdir setAllowsMultipleSelection:NO];
        [exportdir setDirectoryURL:[[NSFileManager defaultManager] URLForDirectory:NSDesktopDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil]];
        
        NSInteger modalResult = [exportdir runModal];
        BOOL result = (modalResult == NSOKButton);
        @try {
            if (result) [self.pathPopUp setURL:[exportdir URL]];
        }
        @catch (NSException *exception) {
            NSLog(@"%@", exception.reason);
        }
        return;
    }
    
    [self.progress setHidden:NO];
    [self.statusLabel setHidden:YES];
    
    // Disable user interaction on EVERYTHING
    [[(OnePDFAppDelegate*)[NSApp delegate] outlineView] setEnabled:NO];
    [self.pathPopUp setEnabled:NO];
    [self.fileNameTxt setEnabled:NO];
    [self.singlePDFOption setEnabled:NO];
    [self.multiplePDFOption setEnabled:NO];
    [self.chapterMarkers setEnabled:NO];
    [self.compression setEnabled:NO];
    [self.metadataPopoverToggle setEnabled:NO];
    [self hideMetadataPopover:self];
    for (NSCell *currentCell in [self.filterMatrix cells])
        [currentCell setEnabled:NO];
    
    NSTreeNode *root = [[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] rootTreeNode];
    // Set PDF document metadata
    NSDictionary *metaDataDict =
    @{
      PDFDocumentTitleAttribute:    [self.titleMeta stringValue],
      PDFDocumentAuthorAttribute:   [self.authorMeta stringValue],
      PDFDocumentSubjectAttribute:  [self.subjectMeta stringValue],
      PDFDocumentKeywordsAttribute: [self.keywordsMeta objectValue] ? [self.keywordsMeta objectValue] : @[],
      PDFDocumentCreatorAttribute:  [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleName"]
    };
    self.overwriteAll = -1;
    BOOL (^shouldOverwrite)(NSString*) = ^(NSString *filePath) {
        if (self.overwriteAll == 0 || self.overwriteAll == 1)
            return (BOOL)self.overwriteAll;
        NSString *fileExists = [NSString stringWithFormat:NSLocalizedString(@"File \"%@\" Exists",nil), [filePath lastPathComponent]];
        
        NSAlert *alert = [NSAlert alertWithMessageText:fileExists
                                         defaultButton:NSLocalizedString(@"No", nil)
                                       alternateButton:NSLocalizedString(@"Yes", nil)
                                           otherButton:nil
                             informativeTextWithFormat:NSLocalizedString(@"Overwrite \"%@\"?",nil), [filePath lastPathComponent]];
        [alert setShowsSuppressionButton:YES];
        [[alert suppressionButton] setTitle:NSLocalizedString(@"Apply to all", nil)];
        
        BOOL overwrite = [alert runModal] == NSAlertAlternateReturn;
        if ([[alert suppressionButton] state] == NSOnState)
            self.overwriteAll = (int)overwrite;
        return overwrite;
    };
    [(OnePDFAppDelegate*)[NSApp delegate] setSuppressErrors:NO];
    void (^errorBlock)(NSError*,NSString*,NSString*) = ^(NSError *error, NSString *msgTxt, NSString *descTxt) {
        if ([(OnePDFAppDelegate*)[NSApp delegate] suppressErrors]) return;
        NSAlert *alert = nil;
        if (error) {
            alert = [NSAlert alertWithError:error];
        } else {
            alert = [NSAlert alertWithMessageText:msgTxt?NSLocalizedString(msgTxt, nil):NSLocalizedString(@"Error", nil)
                                    defaultButton:NSLocalizedString(@"OK", nil)
                                  alternateButton:nil
                                      otherButton:nil
                        informativeTextWithFormat:@"%@",descTxt?NSLocalizedString(descTxt, nil):@""];
            [alert setAlertStyle:NSCriticalAlertStyle];
        }
        
        if ([(OnePDFAppDelegate*)[NSApp delegate] errorsShown] > 1) {
            [alert setShowsSuppressionButton:YES];
            [[alert suppressionButton] setTitle:NSLocalizedString(@"Withhold further errors", nil)];
        }
        
        [alert runModal];
        
        if ([[alert suppressionButton] state] == NSOnState)
            [(OnePDFAppDelegate*)[NSApp delegate] setSuppressErrors:YES];
        
        [(OnePDFAppDelegate*)[NSApp delegate] setErrorsShown:[(OnePDFAppDelegate*)[NSApp delegate] errorsShown] + 1];
    };
    
    NSString *fileName = [[self.fileNameTxt stringValue] isEqualToString:@""] ?
                          NSLocalizedString(@"Output", nil) : [self.fileNameTxt stringValue];
    NSMutableArray *UTIs = [@[] mutableCopy];
    for (NSButtonCell *cell in [self.filterMatrix cells])
        if ([cell state] == NSOffState) [UTIs addObject:[cell identifier]];
    self.currentProcessController =
    [OnePDFProcessController processControllerWithRootTreeNode:root
                                                      fileName:fileName
                                              parentFolderPath:[[self.pathPopUp URL] path]
                                                     singlePDF:[self.singlePDFOption state]==NSOnState
                                                chapterMarkers:[self.chapterMarkers state]==NSOnState
                                                   compression:[self.compression floatValue]
                                            metaDataDictionary:metaDataDict
                                             progressIndicator:self.progress
                                                overwriteBlock:shouldOverwrite
                                                    errorBlock:errorBlock
                                                 UTIsToExclude:[UTIs copy]];
    [self.currentProcessController beginProcess];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(processingCompleted:)
                                                 name:ONEPDF_PROCESS_CONTROLLER_COMPLETE_NOTIFICATION
                                               object:self.currentProcessController];
}

- (void)processingCompleted:(NSDictionary*)userInfo {
    [self.statusLabel setHidden:NO];
    [self.progress setHidden:YES];
    
    // Enable EVERYTHING
    [[(OnePDFAppDelegate*)[NSApp delegate] outlineView] setEnabled:YES];
    [self.pathPopUp setEnabled:YES];
    [self.fileNameTxt setEnabled:YES];
    [self.singlePDFOption setEnabled:YES];
    [self.multiplePDFOption setEnabled:YES];
    [self.chapterMarkers setEnabled:YES];
    [self.compression setEnabled:YES];
    [self.metadataPopoverToggle setEnabled:YES];
    for (NSCell *currentCell in [self.filterMatrix cells])
        [currentCell setEnabled:YES];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:ONEPDF_PROCESS_CONTROLLER_COMPLETE_NOTIFICATION
                                                  object:self.currentProcessController];
    self.overwriteAll = -1;
    self.currentProcessController = nil;
    
    if (NSClassFromString(@"NSUserNotification") != Nil) {
        id notification = [[NSClassFromString(@"NSUserNotification") alloc] init];
        [notification setTitle:NSLocalizedString(@"Your One PDF is complete...", nil)];
        [notification setInformativeText:NSLocalizedString(@"Thanks for using One PDF!", nil)];
        [[NSClassFromString(@"NSUserNotificationCenter")
          performSelector:@selector(defaultUserNotificationCenter)] deliverNotification:notification];
    }
    
    [[NSSound soundNamed:@"Hero"] play];
}

- (BOOL)currentProcessControllerIsRunning {
    return self.currentProcessController.isRunning;
}

#pragma mark Misc

- (void)allElementsRemovedFromTree {
    [self clearList:self];
}

- (BOOL)isUTIFilteredOut:(NSString *)UTI {
    for (NSButtonCell *cell in [self.filterMatrix cells])
        if ([cell state] == NSOffState && [[cell identifier] isEqualToString:UTI])
            return YES;
    return NO;
}

- (IBAction)filterMatrixChanged:(id)sender {
    [[(OnePDFAppDelegate*)[NSApp delegate] outlineView] setNeedsDisplayInRect:[[(OnePDFAppDelegate*)[NSApp delegate] outlineView] rectOfColumn:1]];
    [[(OnePDFAppDelegate*)(OnePDFAppDelegate*)[NSApp delegate] outlineViewController] updateStatusStringPages];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:ONEPDF_OUTLINE_EMPTY_NOTIFICATION
                                                  object:[(OnePDFAppDelegate*)[NSApp delegate] outlineViewController]];
}

@end
