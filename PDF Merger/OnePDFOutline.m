//
//  OnePDFOutline.m
//  One PDF
//
//  Created by Simon on 3/11/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "OnePDFOutline.h"
#import "OnePDFAppDelegate.h"
#import "OnePDFOutlineNodeData.h"

@implementation OnePDFOutline

@synthesize
chapterMarkerColumn = _chapterMarkerColumn,
nameColumn          = _nameColumn,
selectedColumn      = _selectedColumn,
isARowBeingEdited   = _isARowBeingEdited;

- (void)awakeFromNib {
    self.isARowBeingEdited = NO;
    
    if ([[self enclosingScrollView] scrollerStyle] == NSScrollerStyleLegacy)
        [[self tableColumnWithIdentifier:COLUMNID_SCROLLSPACE] setHidden:YES];
    
    // This gets mouseMoved: called when the mouse moves (duh)
    [[self enclosingScrollView] addTrackingArea:
     [[NSTrackingArea alloc] initWithRect:[[self enclosingScrollView] bounds]
                                  options:NSTrackingMouseMoved|NSTrackingActiveInActiveApp
                                    owner:self
                                 userInfo:nil]];
    
    [[[self chapterMarkerColumn] dataCell] setHighlightsBy:NSNoCellMask];
    [[[self selectedColumn] dataCell] setHighlightsBy:NSNoCellMask];
}

#pragma mark Key Events

- (void)keyDown:(NSEvent *)theEvent
{
    NSString* keys = [theEvent charactersIgnoringModifiers];
    unichar firstCharOfKeys = [keys characterAtIndex:0];
    if ([keys isEqual:@" "] && !([theEvent modifierFlags] & NSDeviceIndependentModifierFlagsMask)) { // Space: Quicklook selected
        [(id<OnePDFOutlineController>)[self delegate] togglePreviewPanel:self];
    } else if(firstCharOfKeys==NSEnterCharacter ||
              firstCharOfKeys==NSCarriageReturnCharacter ||
              firstCharOfKeys==NSNewlineCharacter) { // Return/enter: Checkbox
        if ([theEvent modifierFlags] & NSAlternateKeyMask) {
            NSTreeNode *item = [self itemAtRow:[[self selectedRowIndexes] firstIndex]];
            if ([self.delegate respondsToSelector:@selector(outlineView:setObjectValue:forTableColumn:byItem:)])
                [(id)[self delegate] outlineView:self
                                  setObjectValue:@(![[item representedObject] isChapterMarker])
                                  forTableColumn:[self chapterMarkerColumn]
                                          byItem:item];
            else {
                NSIndexSet *sel = [self selectedRowIndexes];
                
                NSUInteger currentIndex = [sel firstIndex];
                while (currentIndex != NSNotFound) {
                    OnePDFOutlineNodeData *item = [[self itemAtRow:currentIndex] representedObject];
                    [item setChapterMarker:![item isChapterMarker]];
                    currentIndex = [sel indexGreaterThanIndex:currentIndex];
                }
                [self reloadData];
            }
        } else if ([theEvent modifierFlags] & NSCommandKeyMask) {
            NSTreeNode *item = [self itemAtRow:[[self selectedRowIndexes] firstIndex]];
            if ([self.delegate respondsToSelector:@selector(outlineView:setObjectValue:forTableColumn:byItem:)])
                [(id)[self delegate] outlineView:self
                                  setObjectValue:@(![[item representedObject] isSelected])
                                  forTableColumn:[self selectedColumn]
                                          byItem:item];
            else {
                NSIndexSet *sel = [self selectedRowIndexes];
                NSUInteger currentIndex = [sel lastIndex];
                while (currentIndex != NSNotFound) {
                    OnePDFOutlineNodeData *item = [[self itemAtRow:currentIndex] representedObject];
                    [item setSelected:![item isSelected]];
                    currentIndex = [sel indexLessThanIndex:currentIndex];
                }
                [self reloadData];
            }
        } else {
            [super keyDown:theEvent];
        }
    } else {
        [super keyDown:theEvent];
    }
}
- (BOOL)performKeyEquivalent:(NSEvent *)theEvent {
    if (!self.isEnabled) return [super performKeyEquivalent:theEvent];
    NSString *chars = [theEvent charactersIgnoringModifiers];
    unichar val = [chars characterAtIndex:0];
    if ([theEvent type] == NSKeyDown && [chars length] == 1) {
        if ((val == 127 || val == 63272) && !self.isARowBeingEdited) { // Delete
            [self delete:self];
            return YES;
        }
    }
    return [super performKeyEquivalent:theEvent];
}

- (void)delete:(id)sender {
    [(id<OnePDFOutlineController>)[self delegate] deleteSelections:self];
}

- (BOOL)isRowBeingHovered:(NSUInteger)row {
    return row == currentRow;
}

- (void)mouseMoved:(NSEvent *)theEvent {
    if ([self isEnabled]) {
        NSPoint pointHovered  = [self convertPoint:[theEvent locationInWindow] fromView:nil];
        NSUInteger oldCurrentRow = currentRow;
        currentRow = [self rowAtPoint:pointHovered];
        if (oldCurrentRow != currentRow) {
            [self setNeedsDisplayInRect:[self rectOfRow:oldCurrentRow]];
            [self setNeedsDisplayInRect:[self rectOfRow:currentRow]];
        }
    }
    [super mouseMoved:theEvent];
}

#pragma mark Quick Look

- (BOOL)acceptsPreviewPanelControl:(QLPreviewPanel *)panel {
    return YES;
}

- (void)beginPreviewPanelControl:(QLPreviewPanel *)panel {
    panel.delegate = (id<OnePDFOutlineController>)self.delegate;
    panel.dataSource = (id<OnePDFOutlineController>)self.delegate;
}

- (void)endPreviewPanelControl:(QLPreviewPanel *)panel {
    panel = nil;
}

@end
