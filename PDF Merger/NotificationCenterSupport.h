//
//  NotificationCenterSupport.h
//  One PDF
//
//  This stops the compiler from complaining when not using the 10.8 SDK...
//  Inspired by http://src.chromium.org/viewvc/chrome/trunk/src/chrome/browser/notifications/notification_ui_manager_mac.mm
//

/* Example usage:
    if (NSClassFromString(@"NSUserNotification") != Nil) {
        id notification = [[NSClassFromString(@"NSUserNotification") alloc] init];
        [notification setTitle:@"Your Notification"];
        [notification setInformativeText:@"Informative Text"];
        [[NSClassFromString(@"NSUserNotificationCenter")
          performSelector:@selector(defaultUserNotificationCenter)] deliverNotification:notification];
    }
 */

#import <Cocoa/Cocoa.h>

#ifndef MAC_OS_X_VERSION_10_8
#if MAC_OS_X_VERSION_MAX_ALLOWED <= MAC_OS_X_VERSION_10_7

@class NSUserNotificationCenter;

@protocol ProtoUserNotification <NSObject>
@property(copy) NSString* title;
@property(copy) NSString* subtitle;
@property(copy) NSString* informativeText;
@property(copy) NSString* actionButtonTitle;
@property(copy) NSDictionary* userInfo;
@property(copy) NSDate* deliveryDate;
@property(copy) NSTimeZone* deliveryTimeZone;
@property(copy) NSDateComponents* deliveryRepeatInterval;
@property(readonly) NSDate* actualDeliveryDate;
@property(readonly, getter=isPresented) BOOL presented;
@property(readonly, getter=isRemote) BOOL remote;
@property(copy) NSString* soundName;
@property BOOL hasActionButton;
@end

@protocol ProtoUserNotificationCenter
+ (NSUserNotificationCenter*)defaultUserNotificationCenter;
@property(assign) id delegate;
@property(copy) NSArray* scheduledNotifications;
- (void)scheduleNotification:(id<ProtoUserNotification>)notification;
- (void)removeScheduledNotification:(id<ProtoUserNotification>)notification;
@property(readonly) NSArray* deliveredNotifications;
- (void)deliverNotification:(id<ProtoUserNotification>)notification;
- (void)removeDeliveredNotification:(id<ProtoUserNotification>)notification;
- (void)removeAllDeliveredNotifications;
@end

#endif
#endif