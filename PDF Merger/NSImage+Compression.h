//
//  NSImage+Compression.h
//  One PDF
//
//  Created by Simon on 26/12/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSImage (Compression)
- (NSImage*)imageCompressedByFactor:(float)factor;
@end
