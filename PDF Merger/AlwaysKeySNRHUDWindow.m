//
//  AlwaysKeyNSWindow.m
//  One PDF
//
//  Created by Simon on 7/06/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <SNRHUDKit/SNRHUDWindow.h>

@interface AlwaysKeySNRHUDWindow : SNRHUDWindow
@end

@implementation AlwaysKeySNRHUDWindow

- (BOOL)isMainWindow {
    return YES;
}

- (BOOL)isKeyWindow {
    return ([NSApp isActive]) ? YES : [super isKeyWindow];
}

@end
