//
//  NSTreeNode+Copying.m
//  One PDF
//
//  Created by Simon on 19/05/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import "NSTreeNode+Copying.h"

@implementation NSTreeNode (Copying)

- (id)copyWithZone:(NSZone *)zone {
    return [self nodeCopy:self];
}

- (NSTreeNode*)nodeCopy:(NSTreeNode*)node {
    id repObj = nil;
    if ([node.representedObject respondsToSelector:@selector(copyWithZone:)])
        repObj = [node.representedObject copy];
    else repObj = node.representedObject;
    NSTreeNode *newNode = [[NSTreeNode alloc] initWithRepresentedObject:repObj];
    NSMutableArray *newNodeKids = [newNode mutableChildNodes];
    for (NSTreeNode *childNode in [node childNodes]) {
        [newNodeKids insertObject:[self nodeCopy:childNode] atIndex:[newNodeKids count]];
    }
    return newNode;
}

@end
