//
//  OnePDFCustomDrawerPanel.h
//  One PDF
//
//  Created by Simon on 26/12/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>

#define ONEPDF_CUSTOM_DRAWER_DEFAULT_RIGHT_SIDE_WIDTH   28.f
#define ONEPDF_CUSTOM_DRAWER_DEFAULT_BUBBLE_RADIUS      16.f
#define ONEPDF_CUSTOM_DRAWER_DEFAULT_CORNER_RADIUS      16.f

@interface OnePDFCustomDrawerPanel : NSPanel

@property (readonly) BOOL isOpen;
@property (readwrite) double rightSideWidth;
@property (readwrite) double bubbleRadius;
@property (readwrite) double cornerRadius;

@property (strong) IBOutlet NSButton *toggleZone;
- (IBAction)toggle:(id)sender;
- (IBAction)open:(id)sender;
- (IBAction)close:(id)sender;

- (void)toggleWithAnimation:(BOOL)anim;
- (void)openWithAnimation:(BOOL)anim;
- (void)closeWithAnimation:(BOOL)anim;

- (void)positionToRightOfWindow:(NSWindow*)window withHeight:(NSInteger)height;

@end
