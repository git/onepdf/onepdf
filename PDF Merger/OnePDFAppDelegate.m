//
//  OnePDFAppDelegate.m
//  One PDF
//
//  Created by Simon on 17/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "OnePDFAppDelegate.h"
#import "OnePDFMainWindowController.h"

#import "OnePDFOutline.h"
#import "OnePDFOutlineController.h"
#import "OnePDFOutlineNodeData.h"

#import "OnePDFFileFolderAdd.h"

@implementation OnePDFAppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    self.suppressErrors = NO;
    if (!self.undoManager) self.undoManager = [[NSUndoManager alloc] init];
    [self.undoManager setLevelsOfUndo:ONEPDF_UNDO_LIMIT];
    self.helpBundle = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"OnePDF" ofType:@"help"]];
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:ONEPDF_FIRST_RUN_COMPLETE] boolValue]) {
        [self firstRun];
    }
}

- (NSString*)pathOfHelpBookPage:(NSString*)page withHash:(NSString*)hash {
    NSString *pathInBundle = [self.helpBundle pathForResource:page ofType:@"html" inDirectory:@"pgs"];
    if (!pathInBundle) pathInBundle = [self.helpBundle pathForResource:page ofType:@"html"];
    if (!pathInBundle) return nil;
    NSString *fileURLString = [[NSURL fileURLWithPath:pathInBundle] absoluteString];
    NSString *helpURLString = [fileURLString stringByReplacingOccurrencesOfString:@"file://"
                                                                       withString:@"help://"];
    if (hash) helpURLString = [NSString stringWithFormat:@"%@#%@", helpURLString, hash];
    return helpURLString;
}

- (void)firstRun {
    [[NSHelpManager sharedHelpManager] registerBooksInBundle:[NSBundle mainBundle]];
    
    NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Thanks for purchasing One PDF!", nil)
                                     defaultButton:NSLocalizedString(@"View Guided Tour", nil)
                                   alternateButton:NSLocalizedString(@"Cancel", nil)
                                       otherButton:nil
                         informativeTextWithFormat:@"%@\n%@",
                      NSLocalizedString(@"Would you like to view our guided tour?", nil),
                      NSLocalizedString(@"You can access it at any time using the Help menu.", nil)];
    
    NSInteger button = [alert runModal];
    if (button == NSAlertDefaultReturn) {
        [[NSHelpManager sharedHelpManager] openHelpAnchor:ONEPDF_HELP_ANCHOR_MAINPAGE inBook:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleHelpBookName"]];
        NSString *helpPath = [self pathOfHelpBookPage:ONEPDF_HELP_PAGE_GUIDEDTOUR withHash:nil];
        [[NSWorkspace sharedWorkspace] performSelector:@selector(openURL:) withObject:[NSURL URLWithString:helpPath] afterDelay:0.3];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:ONEPDF_FIRST_RUN_COMPLETE];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)officeDocumentImportAttempt {
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:ONEPDF_WORD_IMPORT_DIALOG_SUPPRESS] boolValue]) return;
    
    NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Office documents are unsupported", nil)
                                     defaultButton:NSLocalizedString(@"View Import Guide", nil)
                                   alternateButton:NSLocalizedString(@"Skip", nil)
                                       otherButton:nil
                         informativeTextWithFormat:NSLocalizedString(@"Would you like to view the supported method for importing Office documents into One PDF?", nil)];
    [alert setShowsSuppressionButton:YES];
    [[alert suppressionButton] setTitle:NSLocalizedString(@"Do not show this again", nil)];
    //[[alert suppressionButton] setState:NSOnState];
    
    NSInteger button = [alert runModal];
    if (button == NSAlertDefaultReturn) {
        NSString *helpPath = [self pathOfHelpBookPage:ONEPDF_HELP_PAGE_FAQ withHash:ONEPDF_HELP_ANCHOR_FAQ_OFFICEIMPORT];
        [[NSWorkspace sharedWorkspace] openURL:[NSURL URLWithString:helpPath]];
    }
    if ([[alert suppressionButton] state] == NSOnState) {
        [[NSUserDefaults standardUserDefaults] setObject:@(YES) forKey:ONEPDF_WORD_IMPORT_DIALOG_SUPPRESS];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (IBAction)showGuidedTour:(id)sender {
    [[NSHelpManager sharedHelpManager] openHelpAnchor:ONEPDF_HELP_ANCHOR_MAINPAGE inBook:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleHelpBookName"]];
    NSString *helpPath = [self pathOfHelpBookPage:ONEPDF_HELP_PAGE_GUIDEDTOUR withHash:nil];
    [[NSWorkspace sharedWorkspace] performSelector:@selector(openURL:) withObject:[NSURL URLWithString:helpPath] afterDelay:0.3];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)sender {
	return YES;
}

- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window {
    if (!self.undoManager) self.undoManager = [[NSUndoManager alloc] init];
    return self.undoManager;
}

- (BOOL)application:(NSApplication *)sender openFile:(NSString *)filename {
    if (!self.undoManager) self.undoManager = [[NSUndoManager alloc] init];
    [[[(OnePDFAppDelegate*)[NSApp delegate] undoManager] prepareWithInvocationTarget:[self mainWindowController]] clearList:self];
    [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Add File", nil)];
    [self.fileFolderAdd addItemsWithArrayOfFilePaths:@[filename]];
    [self.mainWindowController morphToMainWindow:self];
    return YES;
}

- (void)application:(NSApplication *)sender openFiles:(NSArray *)filenames {
    if (!self.undoManager) self.undoManager = [[NSUndoManager alloc] init];
    [[[(OnePDFAppDelegate*)[NSApp delegate] undoManager] prepareWithInvocationTarget:[self mainWindowController]] clearList:self];
    [[(OnePDFAppDelegate*)[NSApp delegate] undoManager] setActionName:NSLocalizedString(@"Add Files", nil)];
    [self.fileFolderAdd addItemsWithArrayOfFilePaths:filenames];
    [self.mainWindowController morphToMainWindow:self];
}

- (IBAction)undo:(id)sender {
    [self.undoManager undo];
}

- (IBAction)redo:(id)sender {
    [self.undoManager redo];
}

- (BOOL)handleUnlockForDocument:(PDFDocument*)doc {
    BOOL unlocked = NO;
    while (!unlocked) {
        NSString *result = [self presentModalUnlockAlertWithMessageText:[NSString stringWithFormat:
                                                                         NSLocalizedString(@"Password required for PDF document at %@", nil),
                                                                         [[[doc documentURL] path] lastPathComponent]]];
        if (result) {
            unlocked = [doc unlockWithPassword:result];
        } else {
            unlocked = NO;
            break;
        }
    }
    return unlocked;
}

-(NSString*)presentModalUnlockAlertWithMessageText:(NSString*)msgTxt {
    NSAlert *alert = [NSAlert alertWithMessageText:msgTxt
                                     defaultButton:NSLocalizedString(@"Unlock", nil)
                                   alternateButton:NSLocalizedString(@"Cancel", nil)
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    
    NSSecureTextField *input = [[NSSecureTextField alloc] initWithFrame:NSMakeRect(0, 0, 200, 24)];
    
    [alert setAccessoryView:input];
    NSInteger button = [alert runModal];
    if (button == NSAlertDefaultReturn) {
        [input validateEditing];
        return [input stringValue];
    } else {
        return nil;
    }
}

- (void)alertError:(NSError*)error {
    if (self.suppressErrors) return;
    NSAlert *alert = [NSAlert alertWithError:error];
    if (self.errorsShown > 1) {
        [alert setShowsSuppressionButton:YES];
        [[alert suppressionButton] setTitle:NSLocalizedString(@"Withhold further errors", nil)];
    }
    [alert runModal];
    if ([[alert suppressionButton] state] == NSOnState)
        [(OnePDFAppDelegate*)[NSApp delegate] setSuppressErrors:YES];
    self.errorsShown++;
}

- (IBAction)quitApp:(id)sender {
    [self quitApp];
}

- (BOOL)quitApp {
    NSAlert *alert = [NSAlert alertWithMessageText:NSLocalizedString(@"Are you sure?", nil)
                                     defaultButton:NSLocalizedString(@"Cancel", nil)
                                   alternateButton:NSLocalizedString(@"Quit", nil)
                                       otherButton:nil
                         informativeTextWithFormat:@""];
    if ([self.mainWindowController currentProcessControllerIsRunning]) {
        [alert setInformativeText:NSLocalizedString(@"Quitting now will stop the current conversion task.", nil)];
    } else if ([[[self.outlineViewController rootTreeNode] childNodes] count] > 0) {
        [alert setInformativeText:NSLocalizedString(@"Quitting now will remove all files and folders from the list.", nil)];
    } else
        [NSApp terminate:self];
    
    NSInteger button = [alert runModal];
    if (button != NSAlertDefaultReturn)
        [NSApp terminate:self];
    
    return NO;
}

- (BOOL)processing {
    return [self.mainWindowController currentProcessControllerIsRunning];
}

- (BOOL)isOfficeDoc:(CFStringRef)fileUTI {
    if (UTTypeConformsTo(fileUTI, (__bridge CFStringRef)@"com.microsoft.word.doc") ||
        UTTypeConformsTo(fileUTI, (__bridge CFStringRef)@"com.microsoft.excel.xls") ||
        UTTypeConformsTo(fileUTI, (__bridge CFStringRef)@"com.microsoft.powerpoint.ppt") ||
        UTTypeConformsTo(fileUTI, (__bridge CFStringRef)@"org.openxmlformats.openxml")) {
        return YES;
    }
    return NO;
}

- (BOOL)isUTISupported:(CFStringRef)fileUTI {
    if (
            ( // Files and directories (allowed)
             UTTypeConformsTo(fileUTI, kUTTypeImage) ||
             UTTypeConformsTo(fileUTI, kUTTypePDF) ||
             UTTypeConformsTo(fileUTI, kUTTypeDirectory) ||
             UTTypeConformsTo(fileUTI, kUTTypeText) ||
             UTTypeConformsTo(fileUTI, kUTTypeCompositeContent)
             ) &&
            ( // Office documents (denied)
             ![self isOfficeDoc:fileUTI]
             ) &&
            ( // Directories that are packages (denied)
             (!UTTypeConformsTo(fileUTI, kUTTypeBundle) &&
              !UTTypeConformsTo(fileUTI, kUTTypePackage) &&
              !UTTypeConformsTo(fileUTI, kUTTypeArchive)
              ) ||
             ( // Packages that we support (allowed)
              UTTypeConformsTo(fileUTI, kUTTypeWebArchive)
              )
            )
        )
        return YES;
    else return NO;
}

@end
