//
//  OnePDFDropView.m
//  One PDF
//
//  Created by Simon on 22/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "OnePDFDropView.h"
#import "OnePDFAppDelegate.h"

#import "OnePDFFileFolderAdd.h"
#import "OnePDFMainWindowController.h"

@implementation OnePDFDropView

- (id)initWithFrame:(NSRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self registerForDraggedTypes:[NSArray arrayWithObjects:NSFilenamesPboardType, nil]];
        inc=0;
        flipping=NO;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.05
                                                 target:self
                                               selector:@selector(fireTimer)
                                               userInfo:nil
                                                repeats:YES];
        [timer setFireDate:[NSDate distantFuture]];
        timerRunning=NO;
    }
    return self;
}

- (void)startAccel {
    [timer setFireDate:[[NSDate date] dateByAddingTimeInterval:0.05]];
    timerRunning=YES;
    /*
    CPAccelerationTimer *temp = [CPAccelerationTimer accelerationTimerWithTicks:40
                                                                  totalDuration:2
                                                                  controlPoint1:CGPointMake(0, 0)
                                                                  controlPoint2:CGPointMake(1, 1)
                                                                   atEachTickDo:^(NSUInteger i) {
                                                                       [self fireTimer];
                                                                   } completion:^{
                                                                       [timer setFireDate:[[NSDate date] dateByAddingTimeInterval:0.05]];
                                                                   }];
    [temp run];*/
}

- (void)stopAccel {
    [timer setFireDate:[NSDate distantFuture]];
    timerRunning=NO;
    [self setNeedsDisplay:YES];
    /*
    CPAccelerationTimer *temp = [CPAccelerationTimer accelerationTimerWithTicks:40
                                                                  totalDuration:2
                                                                  controlPoint1:CGPointMake(0, 0)
                                                                  controlPoint2:CGPointMake(1, 1)
                                                                   atEachTickDo:^(NSUInteger i) {
                                                                       [self fireTimer];
                                                                   } completion:^{
                                                                       [timer setFireDate:[NSDate distantFuture]];
                                                                   }];
    [temp run];*/
}

- (void)fireTimer {
    inc+=2;
    [self setNeedsDisplay:YES];
}

- (void)drawRect:(NSRect)dirtyRect {
    NSColor *theColor = timerRunning?[NSColor whiteColor]:[NSColor colorWithCalibratedWhite:0.65 alpha:1.0];
    [theColor set];
    
    NSShadow *theShadow = [NSShadow new];
    [theShadow setShadowOffset:NSMakeSize(0.f, -2.f)];
    [theShadow setShadowBlurRadius:4.f];
    [theShadow setShadowColor:[NSColor blackColor]];
    [theShadow set];
    
    NSRect drawingRect = CGRectInset(self.bounds, 33.75, 33.75);//67.5, 67.5);
    
    // Dashed Border
    NSBezierPath *path = [NSBezierPath bezierPathWithRoundedRect:drawingRect xRadius:drawingRect.size.width/10 yRadius:drawingRect.size.width/10];
    CGFloat pattern[2];
    pattern[0] = 36.0; //segment painted with stroke color
    pattern[1] = 8.0; // segment left clear
    [path setLineDash:pattern count:2 phase:inc%44];
    [path setLineWidth:4];
    [path stroke];
    
    CGPoint centerPoint = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    if (0) { // Arrow
        [[NSShadow new] set];
        [theColor set];
        NSRect arrowRect = CGRectOffset(CGRectMake(centerPoint.x, centerPoint.y, drawingRect.size.width/2, drawingRect.size.height/1.75),-drawingRect.size.width/4,-drawingRect.size.height/3.5);
        
        NSBezierPath *mainPath = [NSBezierPath bezierPath];
        [mainPath setLineWidth:arrowRect.size.width/4];
        
        [mainPath moveToPoint:CGPointMake(centerPoint.x, arrowRect.origin.y + arrowRect.size.height)];
        [mainPath lineToPoint:CGPointMake(centerPoint.x, arrowRect.origin.y + arrowRect.size.height/2.5)];
        [mainPath stroke];
        [mainPath lineToPoint:CGPointMake(centerPoint.x, arrowRect.origin.y)];
        
        NSBezierPath *arrowHead = [mainPath bezierPathWithArrowHeadForEndOfLength:arrowRect.size.height/1.75
                                                                            angle:30];
        [arrowHead closePath];
        [arrowHead fill];
        [arrowHead stroke];
    } else { // Text
        NSString *label = flipping?NSLocalizedString(@"Loading...",nil):NSLocalizedString(@"Drop Files Here",nil);
        //NSString *label = timerRunning?@"Drop Files\nHere":@"Drag Files\nHere";
        //NSString *label = NSLocalizedString(@"Drop files here",nil);
        
        NSMutableDictionary *mutableAttributes = [@{
                                                  NSFontAttributeName:[NSFont fontWithName:@"Impact" size:drawingRect.size.width/8],
                                                  NSForegroundColorAttributeName: theColor,
                                                  NSShadowAttributeName: theShadow
                                                  } mutableCopy];
        NSDictionary *attributes = [mutableAttributes copy];
        
        NSMutableAttributedString *attrLabel = [[NSMutableAttributedString alloc] initWithString:label attributes:attributes];
        [attrLabel setAlignment:NSCenterTextAlignment range:NSMakeRange(0, [attrLabel length])];
        NSSize labelSize = attrLabel.size;
        NSRect labelRect = CGRectMake(NSMidX(drawingRect) - (labelSize.width / 2.f),
                                      NSMidY(drawingRect) - (labelSize.height / 2.f),
                                      labelSize.width>NSWidth(drawingRect)-16?NSWidth(drawingRect)-16:labelSize.width,
                                      labelSize.height
                                      );
        [attrLabel drawInRect:NSIntegralRect(labelRect)];
    }
}

// Unused...
- (void)drawArrowWithContext:(CGContextRef)context atPoint:(CGPoint)startPoint withSize:(CGSize)size lineWidth:(float)width arrowHeight:(float)aheight {
    float width_wing = (size.width-width)/2;
    float main = size.height-aheight;
    CGPoint rectangle_points[] =
    {
        CGPointMake(startPoint.x + width_wing, startPoint.y + 0.0),
        CGPointMake(startPoint.x + width_wing, startPoint.y + main),
        CGPointMake(startPoint.x + 0.0, startPoint.y + main), // left point
        CGPointMake(startPoint.x + size.width/2, startPoint.y + size.height),
        
        CGPointMake(startPoint.x + size.width, startPoint.y + main), // right point
        
        CGPointMake(startPoint.x + size.width-width_wing, startPoint.y + main),
        
        CGPointMake(startPoint.x + size.width-width_wing, startPoint.y + 0.0),
        CGPointMake(startPoint.x + width_wing, startPoint.y + 0.0),
    };
    
    CGContextAddLines(context, rectangle_points, 8);
    
    CGContextFillPath(context);
}

- (NSDragOperation)draggingEntered:(id <NSDraggingInfo>)sender {
    if ((NSDragOperationGeneric & [sender draggingSourceOperationMask]) == NSDragOperationGeneric) {
        [self startAccel];
        [[NSCursor dragCopyCursor] set];
        return NSDragOperationGeneric;
    }
    return NSDragOperationNone;
}

- (BOOL)prepareForDragOperation:(id <NSDraggingInfo>)sender {
    return YES;
}

- (void)draggingExited:(id<NSDraggingInfo>)sender {
    [self stopAccel];
    [[NSCursor arrowCursor] set];
}

- (BOOL)performDragOperation:(id <NSDraggingInfo>)sender {
    NSPasteboard *pasteboard = [sender draggingPasteboard];
    
    NSString *theType = [pasteboard availableTypeFromArray:[NSArray arrayWithObject:NSFilenamesPboardType]];
    
    if ([theType isEqualToString:NSFilenamesPboardType]) {
        [self draggingExited:sender];
        flipping=YES;
        [self setNeedsDisplay:YES];
        if ([[(OnePDFAppDelegate*)[NSApp delegate] fileFolderAdd] addItemsWithArrayOfFilePaths:[pasteboard propertyListForType:NSFilenamesPboardType]]) {
            [[(OnePDFAppDelegate*)[NSApp delegate] mainWindowController] morphToMainWindow:self];
        }
        flipping=NO;
        [self setNeedsDisplay:YES];
        return YES;
    }
    [self draggingExited:sender];
    return NO;
    
}

@end
