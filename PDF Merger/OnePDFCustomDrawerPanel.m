//
//  OnePDFCustomDrawerPanel.m
//  One PDF
//
//  Created by Simon on 26/12/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "OnePDFCustomDrawerPanel.h"

@interface OnePDFCustomDrawerPanel()
@property (nonatomic,readwrite) BOOL isOpen;
@end

@implementation OnePDFCustomDrawerPanel

@synthesize
isOpen          = _isOpen,
rightSideWidth  = _rightSideWidth,
bubbleRadius    = _bubbleRadius,
cornerRadius    = _cornerRadius;

- (id)initWithContentRect:(NSRect)contentRect
                styleMask:(NSUInteger)aStyle
                  backing:(NSBackingStoreType)bufferingType
                    defer:(BOOL)flag {
    // Using NSBorderlessWindowMask results in a window without a title bar.
    self = [super initWithContentRect:contentRect styleMask:NSBorderlessWindowMask backing:NSBackingStoreBuffered defer:NO];
    if (self != nil) {
        // Start with no transparency for all drawing into the window
        [self setAlphaValue:1.0];
        // Turn off opacity so that the parts of the window that are not drawn into are transparent.
        [self setOpaque:NO];
        [self setBackgroundColor:[NSColor clearColor]];
        self.rightSideWidth = ONEPDF_CUSTOM_DRAWER_DEFAULT_RIGHT_SIDE_WIDTH;
        self.bubbleRadius = ONEPDF_CUSTOM_DRAWER_DEFAULT_BUBBLE_RADIUS;
        self.cornerRadius = ONEPDF_CUSTOM_DRAWER_DEFAULT_CORNER_RADIUS;
        self.isOpen = NO;
    }
    return self;
}

/*
 Custom windows that use the NSBorderlessWindowMask can't become key by default. Override this method
 so that controls in this window will be enabled.
 */
- (BOOL)canBecomeKeyWindow {
    return YES;
}
- (BOOL)canBecomeMainWindow {
    return NO;
}

- (IBAction)toggle:(id)sender {
    [self toggleWithAnimation:YES];
}

- (IBAction)open:(id)sender {
    [self openWithAnimation:YES];
}

- (IBAction)close:(id)sender {
    [self closeWithAnimation:YES];
}

- (void)toggleWithAnimation:(BOOL)anim {
    self.isOpen?[self closeWithAnimation:anim]:[self openWithAnimation:anim];
}

- (void)openWithAnimation:(BOOL)anim {
    NSRect newFrame = self.frame;
    newFrame.origin.x += newFrame.size.width-self.rightSideWidth;
    [self setFrame:newFrame display:YES animate:anim];
    self.isOpen = YES;
    [self makeKeyWindow];
}

- (void)closeWithAnimation:(BOOL)anim {
    NSRect newFrame = self.frame;
    newFrame.origin.x -= newFrame.size.width-self.rightSideWidth;
    [self setFrame:newFrame display:YES animate:anim];
    self.isOpen = NO;
    [[self parentWindow] makeKeyWindow];
}

- (void)positionToRightOfWindow:(NSWindow*)window withHeight:(NSInteger)height {
    NSRect windowFrame = window.frame;
    NSRect newFrame = self.frame;
    
    newFrame.size.height = height + 4;
    
    newFrame.origin.x = windowFrame.origin.x + windowFrame.size.width;
    newFrame.origin.x -= self.isOpen?0:newFrame.size.width-self.rightSideWidth; // Subtract the correct width
    newFrame.origin.y = windowFrame.origin.y + 20;
    
    [self setFrame:newFrame display:YES animate:NO];
    
}

@end
