//
//  OnePDFMainWindowController.h
//  One PDF
//
//  Created by Simon on 17/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuickLookUI/QuickLookUI.h>
#import "INPopoverController.h"
#import "NSTreeNode+Copying.h"

@class OnePDFFileFolderAdd;
@class OnePDFCustomDrawerPanel;

#import "OnePDFProcessController.h"

#import "NotificationCenterSupport.h"

@interface OnePDFMainWindowController : NSWindowController <NSPopoverDelegate, NSPathControlDelegate, NSOpenSavePanelDelegate, INPopoverControllerDelegate> {
    // Sheet stuff
	NSWindow *sheetWindow;
	NSView *blankingView;
    
    BOOL mainWindowZoomed;
    NSRect mainWindowPrevFrame;
    
    NSSize dragWindowStartingSize;
    NSSize mainWindowStartingSize;
}

@property (strong) IBOutlet NSWindow *dragWindow;                          // Initial Window
    - (void)morphToDragWindow;
@property (strong) IBOutlet NSWindow *mainWindow;                          // Main Window (with outline view)
    - (IBAction)morphToMainWindow:(id)sender;
    @property BOOL flippedToMainWindow;
    @property (weak) IBOutlet NSTextField *statusLabel;                    // The label at the bottom of self.mainWindow, behind the progress bar
    @property (weak) IBOutlet NSProgressIndicator *progress;               // The progress bar at the bottom of self.mainWindow
@property (assign) IBOutlet OnePDFCustomDrawerPanel *customDrawer;         // The custom drawer panel
    @property (weak) IBOutlet NSTextFieldCell *fileNameTxt;                // The filename textbox
    @property (weak) IBOutlet NSPathControl *pathPopUp;                    // The pop-up box for choosing output directory
    @property (weak) IBOutlet NSButtonCell *singlePDFOption;               // The radio button labelled "Single PDF"
    @property (weak) IBOutlet NSButtonCell *multiplePDFOption;             // The radio button labeled "PDF Per Folder"
    @property (weak) IBOutlet NSButtonCell *chapterMarkers;                // The checkbox labelled Chapter Markers
    @property (assign) IBOutlet NSViewController *metadataViewController;  // The view controller for the metadata popover
    @property (strong) INPopoverController *metadataPopoverController;     // The popover for entering metadata
    @property (weak) IBOutlet NSButton *metadataPopoverToggle;             // The toggle for the metadata popover
        - (IBAction)toggleMetadataPopover:(id)sender;
        - (IBAction)showMetadataPopover:(id)sender;
        - (IBAction)hideMetadataPopover:(id)sender;
        @property (weak) IBOutlet NSTextField *titleMeta;                  // The textfield labelled Title in self.metadataPopover
        @property (weak) IBOutlet NSTextField *authorMeta;                 // The textfield labelled Author in self.metadataPopover
        @property (weak) IBOutlet NSTextField *subjectMeta;                // The textfield labelled Subject in self.metadataPopover
        @property (weak) IBOutlet NSTokenField *keywordsMeta;              // The token field labelled Keywords in self.metadataPopover
    @property (weak) IBOutlet NSSlider *compression;                       // The compression slider
    @property (weak) IBOutlet NSTextField *compressionLabel;               // The label under self.compression
    @property (weak) IBOutlet NSMatrix *filterMatrix;                      // The NSMatrix that filters items using the cell identifiers...
    - (BOOL)isUTIFilteredOut:(NSString*)UTI;

-(IBAction)openButtonAction:(id)sender;

- (IBAction)clearButtonAction:(id)sender;
- (IBAction)addButtonAction:(id)sender;
- (IBAction)processButtonAction:(id)sender;
- (IBAction)clearList:(id)sender;

@property (strong) OnePDFProcessController *currentProcessController;
-(BOOL)currentProcessControllerIsRunning;

- (void)allElementsRemovedFromTree;

@end
