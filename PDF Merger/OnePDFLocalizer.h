//
//  OnePDFLocalizer.h
//  One PDF
//
//  Created by Simon on 6/06/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OnePDFLocalizer : NSObject

@property (weak) IBOutlet NSButtonCell          *openFileButton;

@property (weak) IBOutlet NSTextFieldCell       *outputTextField;

@property (weak) IBOutlet NSButtonCell          *outputSinglePDFOption;
@property (weak) IBOutlet NSButtonCell          *outputMultiplePDFOption;
@property (weak) IBOutlet NSButtonCell          *outputChapterMarkerOption;

@property (weak) IBOutlet NSTextFieldCell       *qualityLabel;
@property (weak) IBOutlet NSTextFieldCell       *qualityLabelLowest;
@property (weak) IBOutlet NSTextFieldCell       *qualityLabelHighest;

@property (weak) IBOutlet NSButtonCell          *metadataEditButton;

@property (weak) IBOutlet NSTextFieldCell       *metadataTitleLabel;
@property (weak) IBOutlet NSTextFieldCell       *metadataTitleField;

@property (weak) IBOutlet NSTextFieldCell       *metadataAuthorLabel;
@property (weak) IBOutlet NSTextFieldCell       *metadataAuthorField;

@property (weak) IBOutlet NSTextFieldCell       *metadataSubjectLabel;
@property (weak) IBOutlet NSTextFieldCell       *metadataSubjectField;

@property (weak) IBOutlet NSTextFieldCell       *metadataKeywordsLabel;
@property (weak) IBOutlet NSTokenFieldCell      *metadataKeywordsField;

@property (weak) IBOutlet NSButtonCell          *metadataDoneButton;

@property (weak) IBOutlet NSTextFieldCell       *filteringLabel;

// Accessibility
@property (weak) IBOutlet NSView                *dropView;

@property (weak) IBOutlet NSButtonCell          *clearListButton;
@property (weak) IBOutlet NSButtonCell          *addFilesButton;
@property (weak) IBOutlet NSButtonCell          *processButton;

@property (weak) IBOutlet NSButtonCell          *chapterMarkerCell;
@property (weak) IBOutlet NSButtonCell          *excludeIncludeCell;

@property (weak) IBOutlet NSProgressIndicator   *progressBar;

@property (weak) IBOutlet NSButtonCell          *advancedOptionsToggle;

// Main Menu
@property (weak) IBOutlet NSMenuItem            *menuProcess;
@property (weak) IBOutlet NSMenuItem            *menuAddFiles;
@property (weak) IBOutlet NSMenuItem            *menuClose;
@property (weak) IBOutlet NSMenuItem            *menuAdvancedOptionsToggle;

@property (weak) IBOutlet NSMenuItem            *menuGuidedTour;


@end
