//
//  OnePDFDropView.h
//  One PDF
//
//  Created by Simon on 22/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>
//#import "CPAccelerationTimer.h"
#import "AJHBezierUtils.h"

// Can't be bothered doing stuff with properties here...
@interface OnePDFDropView : NSView {
    NSUInteger inc;
    NSTimer *timer;
    BOOL timerRunning;
    float interval;
    BOOL flipping;
}
@end
