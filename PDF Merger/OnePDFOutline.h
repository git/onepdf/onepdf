//
//  OnePDFOutline.h
//  One PDF
//
//  Created by Simon on 3/11/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuickLookUI/QuickLookUI.h>
#import <SNRHUDKit/SNRHUDOutlineView.h>

#define LOCAL_REORDER_PASTEBOARD_TYPE @"OnePDFPboardType"

// Column Identifiers
#define COLUMNID_IS_CHAPTER_MARKER    @"IsChapterColumn"    // Uses NSButtonCells
#define COLUMNID_NAME                 @"NameColumn"         // Uses ImageAndTextCell cells
#define COLUMNID_SELECTED             @"SelectedColumn"     // Uses NSButtonCells
#define COLUMNID_SCROLLSPACE          @"ScrollSpaceColumn"  // Uses NSCellWithoutHighlight cells

@protocol OnePDFOutlineController <NSObject, QLPreviewPanelDataSource, QLPreviewPanelDelegate>
- (void)deleteSelections:(id)sender;
- (IBAction)togglePreviewPanel:(id)previewPanel;
- (IBAction)refreshPreviewPanel:(id)previewPanel;
@end

@interface OnePDFOutline : SNRHUDOutlineView {
    NSUInteger currentRow;
}

@property (weak) IBOutlet NSTableColumn *chapterMarkerColumn;
@property (weak) IBOutlet NSTableColumn *nameColumn;
@property (weak) IBOutlet NSTableColumn *selectedColumn;

@property BOOL isARowBeingEdited;

- (BOOL)isRowBeingHovered:(NSUInteger)row;

@end
