//
//  OnePDFLocalizer.m
//  One PDF
//
//  Created by Simon on 6/06/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import "OnePDFLocalizer.h"

@implementation OnePDFLocalizer

- (void)awakeFromNib {
    NSArray *allTheItems = @[
                             self.openFileButton,
                             self.outputTextField,
                             self.outputSinglePDFOption,
                             self.outputMultiplePDFOption,
                             self.outputChapterMarkerOption,
                             self.qualityLabel,
                             self.qualityLabelLowest,
                             self.qualityLabelHighest,
                             self.metadataEditButton,
                             self.metadataTitleLabel,
                             self.metadataTitleField,
                             self.metadataAuthorLabel,
                             self.metadataAuthorField,
                             self.metadataSubjectLabel,
                             self.metadataSubjectField,
                             self.metadataKeywordsLabel,
                             self.metadataKeywordsField,
                             self.metadataDoneButton,
                             self.filteringLabel,
                             // Accessibility
                             /*
                                 self.dropView,
                                 self.clearListButton,
                                 self.addFilesButton,
                                 self.processButton,
                                 self.chapterMarkerCell,
                                 self.excludeIncludeCell,
                                 self.progressBar,
                                 self.advancedOptionsToggle
                              */
                             // Main Menu
                             self.menuProcess,
                             self.menuAddFiles,
                             self.menuClose,
                             self.menuAdvancedOptionsToggle,
                             self.menuGuidedTour,
                             ];
    for (id item in allTheItems) {
        // Preserving the text color, otherwise it might return to the default...
        NSColor *textColor = nil;
        if ([item respondsToSelector:@selector(textColor)] &&
            [item respondsToSelector:@selector(setTextColor:)])
            textColor = [item textColor];
        
        // Translate stringValue
        if ([item respondsToSelector:@selector(stringValue)] &&
            [item respondsToSelector:@selector(setStringValue:)] &&
            ![[item stringValue] isEqualToString:@""])
            [item setStringValue:NSLocalizedString([item stringValue], nil)];
        
        // Translate placeholderString
        if ([item respondsToSelector:@selector(placeholderString)] &&
            [item respondsToSelector:@selector(setPlaceholderString:)] &&
            ![[item placeholderString] isEqualToString:@""])
            [item setPlaceholderString:NSLocalizedString([item placeholderString], nil)];
        
        // Translate title
        if ([item respondsToSelector:@selector(title)] &&
            [item respondsToSelector:@selector(setTitle:)] &&
            ![[item title] isEqualToString:@""])
            [item setTitle:NSLocalizedString([item title], nil)];
        
        // Set the textColor that we saved earlier
        if (textColor) [item setTextColor:textColor];
    }
}

@end
