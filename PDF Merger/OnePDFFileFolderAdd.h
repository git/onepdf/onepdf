//
//  OnePDFFileFolderAdd.h
//  One PDF
//
//  Created by Simon on 25/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import <QuartzCore/QuartzCore.h>

@interface OnePDFFileFolderAdd : NSObject <NSOpenSavePanelDelegate>
- (IBAction)doOpen:(id)sender;
- (BOOL)doOpen;

- (BOOL)addItemWithFileURL:(NSURL*)url calcPageNumbers:(BOOL)recalc;
- (BOOL)addItemsWithArrayOfFileURLs:(NSArray*)urlArray;
- (BOOL)addItemsWithArrayOfFilePaths:(NSArray*)pathArray;

@end
