//
//  constants.h
//  One PDF
//
//  Created by Simon on 13/06/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#ifndef One_PDF_constants_h
#define One_PDF_constants_h

#define ONEPDF_PAPER_SIZE           CGRectMake(0,0,595,842)
#define ONEPDF_LARGE_FILE_SIZE      40*(1024*1024)
#define ONEPDF_LARGE_FILE_AMOUNT    500
#define ONEPDF_LARGE_PAGE_AMOUNT    1000

#define ONEPDF_PROCESS_CONTROLLER_COMPLETE_NOTIFICATION     @"OnePDFProcComplete"
#define ONEPDF_OUTLINE_EMPTY_NOTIFICATION                   @"OnePDFOutlineEmpty"

#define ONEPDF_ILLEGAL_FILE_NAME_CHARS [NSCharacterSet characterSetWithCharactersInString:@":/\\?*\""]

#define ONEPDF_FIRST_RUN_COMPLETE @"FirstRunComplete"
#define ONEPDF_WORD_IMPORT_DIALOG_SUPPRESS @"WordImportDialogSuppress"

#define ONEPDF_UNDO_LIMIT 20

// Help Book

#define ONEPDF_HELP_PAGE_MAINPAGE       @"OnePDF"
#define ONEPDF_HELP_ANCHOR_MAINPAGE     @"mainpage"

#define ONEPDF_HELP_PAGE_WHATISONEPDF   @"whatisonepdf"
#define ONEPDF_HELP_ANCHOR_WHATISONEPDF @"whatisonepdf"
#define ONEPDF_HELP_PAGE_GUIDEDTOUR     @"guidedtour"
#define ONEPDF_HELP_ANCHOR_GUIDEDTOUR   @"guidedtour"

#define ONEPDF_HELP_PAGE_PDFCONVIEW     @"thepdfconstructorview"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEW   @"pdfconstructorview"

#define ONEPDF_HELP_PAGE_PDFCONVIEWTASKS                    @"pdfconstructorviewtasks"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS                  @"pdfconstructorviewtasks"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS_SELECTION        @"itemselection"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS_RENAMING         @"itemrenaming"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS_REARRANGING      @"itemrearranging"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS_EXCLUDE_INCLUDE  @"itemexcludeinclude"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS_REMOVE           @"itemremove"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS_QUICKLOOK        @"itemquicklook"
#define ONEPDF_HELP_ANCHOR_PDFCONVIEWTASKS_SPLITTING        @"pdfsplitting"

#define ONEPDF_HELP_PAGE_UNDOREDO       @"undoredo"
#define ONEPDF_HELP_ANCHOR_UNDOREDO     @"undoredo"

#define ONEPDF_HELP_PAGE_FILEIMPORT     @"fileimport"
#define ONEPDF_HELP_ANCHOR_FILEIMPORT   @"fileimport"

#define ONEPDF_HELP_PAGE_BOOKMARKS      @"bookmarks"
#define ONEPDF_HELP_ANCHOR_BOOKMARKS    @"bookmarks"

#define ONEPDF_HELP_PAGE_PROCESSING     @"processing"
#define ONEPDF_HELP_ANCHOR_PROCESSING   @"processing"

#define ONEPDF_HELP_PAGE_ADVANCEDOPTIONS                        @"advancedoptions"
#define ONEPDF_HELP_ANCHOR_ADVANCEDOPTIONS                      @"advancedoptions"
#define ONEPDF_HELP_ANCHOR_ADVANCEDOPTIONS_OUTPUTLOC            @"outputloc"
#define ONEPDF_HELP_ANCHOR_ADVANCEDOPTIONS_SINGLEPERFOLDERPDF   @"singleperfolderpdf"
#define ONEPDF_HELP_ANCHOR_ADVANCEDOPTIONS_QUALITY              @"pdfquality"
#define ONEPDF_HELP_ANCHOR_ADVANCEDOPTIONS_METADATA             @"metadata"
#define ONEPDF_HELP_ANCHOR_ADVANCEDOPTIONS_FILTERING            @"filtering"

#define ONEPDF_HELP_PAGE_FAQ                @"onepdffaq"
#define ONEPDF_HELP_ANCHOR_FAQ              @"onepdffaq"
#define ONEPDF_HELP_ANCHOR_FAQ_FILETYPES    @"filetypes"
#define ONEPDF_HELP_ANCHOR_FAQ_OFFICEIMPORT @"officeimport"
#define ONEPDF_HELP_ANCHOR_FAQ_KEYSHORTCUTS @"keyboardshortcuts"

#endif
