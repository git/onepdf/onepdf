//
//  NSTreeNode+Copying.h
//  One PDF
//
//  Created by Simon on 19/05/13.
//  Copyright (c) 2013 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface NSTreeNode (Copying) <NSCopying>

@end
