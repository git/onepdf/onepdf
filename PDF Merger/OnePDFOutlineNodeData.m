/*
     File: SimpleNodeData.m
 Abstract: Simple object model implementation.
  Version: 1.2
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2012 Apple Inc. All Rights Reserved.
 
*/

#import "OnePDFOutlineNodeData.h"

@interface OnePDFOutlineNodeData ()
@property (nonatomic, readwrite) NSInteger numberOfPages;
@property BOOL *cancelCalcFlag;
@end

@implementation OnePDFOutlineNodeData

#pragma mark -

- (id)init {
    self = [super init];
    _name = @"Untitled";
    _kind = nil;
    _path = nil;
    
    _directory = NO;
    _bookmark = NO;
    
    _selected = YES;
    _expanded = NO;
    
    _chapterMarker = YES;
    
    _numberOfPages = -1;
    
    _PDF = nil;
    return self;
}

- (id)initWithName:(NSString *)aName {
    self = [self init];
    _name = aName;
    return self;
}

- (id)copyWithZone:(NSZone *)zone {
    OnePDFOutlineNodeData *newNodeData = [[[self class] allocWithZone:zone] init];
    if (newNodeData) {
        newNodeData->_name = [_name copy];
        newNodeData->_kind = [_kind copy];
        newNodeData->_path = [_path copy];
        newNodeData->_selected = _selected;
        newNodeData->_expanded = _expanded;
        
        newNodeData->_directory = _directory;
        newNodeData->_bookmark = _bookmark;
        
        newNodeData->_chapterMarker = _chapterMarker;
        
        newNodeData->_numberOfPages = _numberOfPages;
        
        newNodeData->_PDF = _PDF;
    }
    return newNodeData;
}


+ (OnePDFOutlineNodeData *)nodeDataWithName:(NSString *)name {
    return [[OnePDFOutlineNodeData alloc] initWithName:name];
}


- (NSComparisonResult)compare:(id)anOther {
    // We want the data to be sorted by name, so we compare [self name] to [other name]
    if ([anOther isKindOfClass:[OnePDFOutlineNodeData class]]) {
        OnePDFOutlineNodeData *other = (OnePDFOutlineNodeData *)anOther;
        return [self.name compare:[other name]];
    } else {
        return NSOrderedAscending;
    }
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%@ - '%@' kind: %@ path: %@ directory: %d, bookmark: %d, chapterMarker: %d selected:%d",
            [super description],
            self.name,
            self.kind,
            self.path,
            self.directory,
            self.bookmark,
            self.chapterMarker,
            self.selected
            ];
}

#pragma mark -
#pragma mark NSPasteboardWriting support

- (NSArray *)writableTypesForPasteboard:(NSPasteboard *)pasteboard {
    // These are the types we can write.
    NSArray *ourTypes = [NSArray arrayWithObjects:NSPasteboardTypeString, nil];
    // Also include the images on the pasteboard too!
    NSArray *imageTypes = [self.image writableTypesForPasteboard:pasteboard];
    if (imageTypes) {
        ourTypes = [ourTypes arrayByAddingObjectsFromArray:imageTypes];
    }
    return ourTypes;
}

- (NSPasteboardWritingOptions)writingOptionsForType:(NSString *)type pasteboard:(NSPasteboard *)pasteboard {
    if ([type isEqualToString:NSPasteboardTypeString]) {
        return 0;
    }
    // Everything else is delegated to the image
    if ([self.image respondsToSelector:@selector(writingOptionsForType:pasteboard:)]) {            
        return [self.image writingOptionsForType:type pasteboard:pasteboard];
    }
    
    return 0;
}

- (id)pasteboardPropertyListForType:(NSString *)type {
    if ([type isEqualToString:NSPasteboardTypeString]) {
        return self.name;
    } else {
        return [self.image pasteboardPropertyListForType:type];
    }
}

#pragma mark -
#pragma mark  NSPasteboardReading support

+ (NSArray *)readableTypesForPasteboard:(NSPasteboard *)pasteboard {
    // We allow creation from URLs so Finder items can be dragged to us
    return [NSArray arrayWithObjects:(id)kUTTypeURL, nil];
}

+ (NSPasteboardReadingOptions)readingOptionsForType:(NSString *)type pasteboard:(NSPasteboard *)pasteboard {
    CFStringRef typeRef = (__bridge CFStringRef)type;
    if ([type isEqualToString:NSPasteboardTypeString] || UTTypeConformsTo(typeRef, kUTTypeURL)) {
        return NSPasteboardReadingAsString;
    } else {
        return NSPasteboardReadingAsData; 
    }
}

- (id)initWithPasteboardPropertyList:(id)propertyList ofType:(NSString *)type {
    // See if an NSURL can be created from this type
    if (UTTypeConformsTo((__bridge CFStringRef)type, kUTTypeURL)) {
        // It does, so create a URL and use that to initialize our properties
        NSURL *url = [[NSURL alloc] initWithPasteboardPropertyList:propertyList ofType:type];
        self = [super init];
        // This makes a string without the extension...
        //self.name = [[url lastPathComponent] stringByDeletingPathExtension];
        
        // Make sure we have a name
        //if (self.name == nil) {
            _name = [url lastPathComponent];
            if (_name == nil) {
                _name = [url path];
                if (_name == nil) {
                    _name = @"Untitled";
                }
            }
        //}
        _selected = YES;
        //self.kind = [url pathExtension];
        NSString *UTIstr = @"";
        [url getResourceValue:&UTIstr forKey:NSURLTypeIdentifierKey error:NULL];
        _kind = UTIstr;
        _path = [[url path] stringByResolvingSymlinksInPath];
        
        _numberOfPages = -1;
        
        // See if the URL was a container; if so, make us marked as a container too
        if (UTTypeConformsTo((__bridge CFStringRef)(UTIstr), kUTTypeDirectory) &&
            !UTTypeConformsTo((__bridge CFStringRef)(UTIstr), kUTTypeBundle) &&
            !UTTypeConformsTo((__bridge CFStringRef)(UTIstr), kUTTypePackage) &&
            !UTTypeConformsTo((__bridge CFStringRef)(UTIstr), kUTTypeArchive)) {            
            _directory = YES;
            _chapterMarker = YES;
        } else {            
            _directory = NO;
            _chapterMarker = NO;
        } 
        _bookmark = NO;

        /*NSImage *localImage;
        if ([url getResourceValue:&localImage forKey:NSURLEffectiveIconKey error:NULL] && localImage) {
            self.image = localImage;
        }*/
        
    } else {
        NSLog(@"type not supported");
        return nil;
    }        
    return self;
}

- (NSInteger)numberOfPages {
    if (_numberOfPages < 0) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:self.path]) {
            
            NSInteger cnt = 1;
            
            if (UTTypeConformsTo((__bridge CFStringRef)(self.kind), kUTTypeDirectory)) cnt = 0;
            
            else if (UTTypeConformsTo((__bridge CFStringRef)(self.kind), kUTTypeImage)) cnt = 1;
            
            else if (UTTypeConformsTo((__bridge CFStringRef)(self.kind), kUTTypePDF)) {
                PDFDocument *pdf = self.PDF ? self.PDF : [[PDFDocument alloc] initWithURL:[NSURL fileURLWithPath:self.path]];
                cnt = [pdf pageCount];
            }
            
            else if (UTTypeConformsTo((__bridge CFStringRef)(self.kind), kUTTypeText)) {
                NSAttributedString *content = [[NSAttributedString alloc] initWithPath:self.path documentAttributes:nil];
                
                if (self.cancelCalcFlag) return -1;
                
                // Allocate a textview
                NSTextView *txtView = [[NSTextView alloc] initWithFrame:ONEPDF_PAPER_SIZE]; // Approx A4 size
                [[txtView textStorage] setAttributedString:content];
                
                if (self.cancelCalcFlag) return -1;
                
                // Settings for print operation
                NSPrintInfo *sharedInfo = [NSPrintInfo sharedPrintInfo];
                NSMutableDictionary *printInfoDict = [NSMutableDictionary dictionaryWithDictionary:[sharedInfo dictionary]];
                [printInfoDict setObject:NSPrintSaveJob forKey:NSPrintJobDisposition];
                NSPrintInfo *printInfo = [[NSPrintInfo alloc] initWithDictionary:printInfoDict];
                [printInfo setHorizontalPagination:NSFitPagination];
                [printInfo setVerticalPagination:NSAutoPagination];
                [printInfo setVerticallyCentered:NO];
                
                if (self.cancelCalcFlag) return -1;
                
                NSInteger len = [[NSPrintOperation printOperationWithView:txtView printInfo:printInfo] pageRange].length;
                cnt = (len != NSIntegerMax)? len : 0;
            }
            _numberOfPages = cnt;
        } else if (self.PDF) {
            _numberOfPages = [self.PDF pageCount];
        } else _numberOfPages = 0;
    }
    return _numberOfPages;
}

- (NSInteger)numberOfPagesWithCancellationFlag:(BOOL*)flag {
    self.cancelCalcFlag = flag;
    NSInteger res = [self numberOfPages];
    self.cancelCalcFlag = NULL;
    return res;
}

@end

