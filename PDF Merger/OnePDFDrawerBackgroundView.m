//
//  OnePDFDrawerBackgroundView.m
//  One PDF
//
//  Created by Simon on 26/12/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import "OnePDFDrawerBackgroundView.h"

#define ONEPDF_CUSTOM_DRAWER_BACKGROUND_COLOR [NSColor colorWithDeviceWhite:0.150 alpha:1]

@interface OnePDFDrawerBackgroundView ()

@property double rightSideWidth;
@property double bubbleRadius;
@property double cornerRadius;

@end

@implementation OnePDFDrawerBackgroundView

- (void)awakeFromNib {
    self.rightSideWidth = ONEPDF_CUSTOM_DRAWER_DEFAULT_RIGHT_SIDE_WIDTH;
    self.bubbleRadius = ONEPDF_CUSTOM_DRAWER_DEFAULT_BUBBLE_RADIUS;
    self.cornerRadius = ONEPDF_CUSTOM_DRAWER_DEFAULT_CORNER_RADIUS;
    
    if ([self.window isKindOfClass:[OnePDFCustomDrawerPanel class]]) {
        self.rightSideWidth = ((OnePDFCustomDrawerPanel*)self.window).rightSideWidth;
        self.bubbleRadius = ((OnePDFCustomDrawerPanel*)self.window).bubbleRadius;
        self.cornerRadius = ((OnePDFCustomDrawerPanel*)self.window).cornerRadius;
    }
}

- (void)drawRect:(NSRect)dirtyRect {
    NSRect dr = [self bounds];
    dr.size.width = dr.size.width - self.bubbleRadius - 2;
    dr.size.height = dr.size.height - 6;
    dr.origin.y = dr.origin.y + 2;
    
    NSBezierPath *path = [self bgPathInsideRect:dr];
    
    [NSGraphicsContext saveGraphicsState];
    
    [ONEPDF_CUSTOM_DRAWER_BACKGROUND_COLOR set];
    [path addClip];
    [NSBezierPath fillRect:self.bounds];
    
    [NSGraphicsContext restoreGraphicsState];
    [NSGraphicsContext saveGraphicsState];
    
    [[NSColor blackColor] set];
    [path stroke];
    
    NSImage *prefImage = [NSImage imageNamed:@"settings"];
    CGPoint bubbleCenter = CGPointMake(dr.size.width + 1, dr.origin.y + dr.size.height/2);
    NSRect newImageRect = CGRectOffset(CGRectMake(bubbleCenter.x, bubbleCenter.y, 16, 16),-8,-8);
    [prefImage drawInRect:newImageRect fromRect:NSZeroRect operation:NSCompositeSourceOver fraction:1.0];
    
    [NSGraphicsContext restoreGraphicsState];
}

- (NSBezierPath*)bgPathInsideRect:(NSRect)dr {
    NSBezierPath *path = [NSBezierPath bezierPath];
    [path moveToPoint:CGPointMake(dr.origin.x, dr.origin.y)];
    [path lineToPoint:CGPointMake(dr.size.width - self.cornerRadius, dr.origin.y)];
    
    [path curveToPoint:CGPointMake(dr.size.width, dr.origin.y + self.cornerRadius)
         controlPoint1:CGPointMake(dr.size.width - self.cornerRadius/2, dr.origin.y)
         controlPoint2:CGPointMake(dr.size.width, dr.origin.y + self.cornerRadius/2)];
    
    [path lineToPoint:CGPointMake(dr.size.width, dr.origin.y + dr.size.height/2 - self.bubbleRadius)];
    
    [path curveToPoint:CGPointMake(dr.size.width + self.bubbleRadius, dr.origin.y + dr.size.height/2)
         controlPoint1:CGPointMake(dr.size.width + self.bubbleRadius/2, dr.origin.y + dr.size.height/2 - self.bubbleRadius)
         controlPoint2:CGPointMake(dr.size.width + self.bubbleRadius, dr.origin.y + dr.size.height/2 - self.bubbleRadius/2)];
    
    [path curveToPoint:CGPointMake(dr.size.width, dr.origin.y + dr.size.height/2 + self.bubbleRadius)
         controlPoint1:CGPointMake(dr.size.width + self.bubbleRadius, dr.origin.y + dr.size.height/2 + self.bubbleRadius/2)
         controlPoint2:CGPointMake(dr.size.width + self.bubbleRadius/2, dr.origin.y + dr.size.height/2 + self.bubbleRadius)];
    
    [path lineToPoint:CGPointMake(dr.size.width, dr.origin.y + dr.size.height - self.cornerRadius)];
    
    [path curveToPoint:CGPointMake(dr.size.width - self.cornerRadius, dr.origin.y + dr.size.height)
         controlPoint1:CGPointMake(dr.size.width, dr.origin.y + dr.size.height - self.cornerRadius/2)
         controlPoint2:CGPointMake(dr.size.width - self.cornerRadius/2, dr.origin.y + dr.size.height)];
    
    [path lineToPoint:CGPointMake(dr.origin.x, dr.origin.y + dr.size.height)];
    return path;
}

@end
