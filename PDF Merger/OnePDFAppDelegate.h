//
//  OnePDFAppDelegate.h
//  One PDF
//
//  Created by Simon on 17/10/12.
//  Copyright (c) 2012 Nourishing Media. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@class OnePDFMainWindowController;
@class OnePDFOutline;
@class OnePDFOutlineController;
@class OnePDFFileFolderAdd;

@class PDFDocument;

@interface OnePDFAppDelegate : NSObject <NSApplicationDelegate>

@property (unsafe_unretained) IBOutlet NSWindow *window;
@property (unsafe_unretained) IBOutlet OnePDFMainWindowController *mainWindowController;
@property (unsafe_unretained) IBOutlet OnePDFOutline *outlineView;
@property (unsafe_unretained) IBOutlet OnePDFOutlineController *outlineViewController;
@property (unsafe_unretained) IBOutlet OnePDFFileFolderAdd *fileFolderAdd;
@property (unsafe_unretained) IBOutlet NSPathControl *pathPopUp;

@property (strong) NSBundle *helpBundle;
- (NSString*)pathOfHelpBookPage:(NSString*)page withHash:(NSString*)hash;

- (void)firstRun;
- (void)officeDocumentImportAttempt;
- (IBAction)showGuidedTour:(id)sender;

- (void)alertError:(NSError*)error;
@property NSUInteger errorsShown;
@property BOOL suppressErrors;

@property (strong) NSUndoManager *undoManager;

- (BOOL)isOfficeDoc:(CFStringRef)fileUTI;
- (BOOL)isUTISupported:(CFStringRef)fileUTI;

- (BOOL)handleUnlockForDocument:(PDFDocument*)doc;

- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window;
- (IBAction)undo:(id)sender;
- (IBAction)redo:(id)sender;

- (IBAction)quitApp:(id)sender;
- (BOOL)quitApp;

- (BOOL)processing;

@end
