#One PDF

The best and easiest way to merge your random files into a single PDF. →→→ SPECIAL INTRODUCTORY PRICE ←←←

Are you tired of folders full of individual, unbound comic pages? Fed up with documents being unnecessarily divided?
Then say hello to *One PDF*; no confusing mumbo-jumbo, just the cleanest and easiest-to-use PDF merging app we could think up. Enjoy.

Features:

 - Simple, two-step PDF creation, just drag in your files, rearrange and go
 - Works with images, text files, other PDFs and more
 - Fast and easy chapter markers to organise larger files
 - Quickly exclude unwanted files that got mixed in with the files you want
 - Split PDFs within the app to rearrange or remove unwanted pages
 - QuickLook and keyboard shortcuts included to make things even faster
 - A special drawer full of extra options for control freaks

##Reviews

Looks great, works great - by *quinndupont* - Apr 2, 2016
> Allows you to easily (rearrange and) join PDFs, and includes a couple of nice extra features, such as: set metadata, create chapter markers, and filter images/quality. As best I can tell, it performs as well as the $20 apps, but works and looks much better than the free ones. Highly recommended!

One PDF - by *cfm0743* - Dec 29, 2014
> I have used a similar utility in windows. Transferring files to windows and back again was such a hassle. This utility saves me big time (very fast), reduces possible errors and lost files are reduced to zero. This utility is quick and efficient! I have sveral thousand PDF’s that needs to be combined into several folders and this utility handles them with no concern. Great utility!! Sorting alpha - numeric files is easy by dragging the files in the order I want.

VERY useful - by *Helmutlord* - Jan 10, 2015
> I’ve used this program at least once a week since I got it - often much more. Great value for money, and something I can’t do without anymore!

Awesome - by *Luke Orrin* - Dec 21, 2014
> Well, it’s exactly what you said. It’s clean, straight-forward and just works. Cheers, guys.
